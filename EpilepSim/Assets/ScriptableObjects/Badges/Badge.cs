using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Badge", menuName = "ScriptableObject/Create Badge")]
public class Badge : ScriptableObject
{
    public int id;
    public Sprite logo;
    public string title;
    [TextArea]
    public string description;
    public bool earn;
    public int award;
    public int level;
    public bool unlock;
}
