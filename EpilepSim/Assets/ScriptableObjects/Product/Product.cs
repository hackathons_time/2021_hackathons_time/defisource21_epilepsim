using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Product", menuName = "ScriptableObject/Create Product")]
public class Product : ScriptableObject
{
    public int id;
    public Sprite logo;
    public string title;
    [TextArea]
    public string description;
    public float price;
}