using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tips", menuName = "ScriptableObject/Create Tips Profil")]
public class TipsProfil : ScriptableObject
{
    public int id;
    public Color color;
    public string title;
    [TextArea]
    public string description;
}
