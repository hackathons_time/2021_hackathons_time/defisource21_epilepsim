using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Quest", menuName = "ScriptableObject/Create Quest Profil")]

public class QuestProfil : ScriptableObject
{
    public int id;
    public string title;
    [TextArea]
    public string description;
    public int award;
}
