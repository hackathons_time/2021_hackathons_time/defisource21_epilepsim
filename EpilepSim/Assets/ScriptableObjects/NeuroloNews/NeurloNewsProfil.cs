using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New NeuroloNews", menuName = "ScriptableObject/Create NeuroloNews")]
public class NeurloNewsProfil : ScriptableObject
{
    public int id;
    public string title;
    [TextArea]
    public string description;
    public bool isUnlocked;
    public int date;
    public int heure;
}
