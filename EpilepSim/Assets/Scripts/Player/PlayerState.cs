using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerState : MonoBehaviour
{
    public enum States { IDLE, GAMING, TV, SLEEPING, CRISIS, PAUSE, TALKING, ACTIVITYFINISHED, BOARD, SHOPPING };
    public States currentState = States.IDLE;

    [SerializeField] private GameObject uiElements;
    [SerializeField] private GameObject paramElements;
    [SerializeField] private GameObject eventSystem; // r�cup�rer le script ActivityManager

    private SaveLastPosition saveLastPosition;

    public int crtBeuri = -100;

    private void Awake()
    {
        // get reference
        saveLastPosition = transform.GetComponent<SaveLastPosition>();

        //Check if data existed 
        if (PlayerPrefs.HasKey("HaveData"))
        {
            //Check Session to use correctly the TPpoint except when you come from menu 
            if (!PlayerPrefs.HasKey("Session"))
            {
                PlayerPrefs.SetInt("Session", 1);
                //Check if LastPlayerPosition existed and change player position
                if (PlayerPrefs.HasKey("LastPlayerPosition"))
                {
                    Debug.Log("JZPlayerPosition forced");
                    Vector3 lastPosiiton = GetComponent<SaveLastPosition>().LastPostionPlayer(PlayerPrefs.GetString("LastPlayerPosition"));
                    
                    transform.localPosition = new Vector3(lastPosiiton.x, lastPosiiton.y, transform.position.z);
                }
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {

        //Visual
        activateAll(true);

       transform.GetChild(0).gameObject.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        //crtBeuri = PlayerPrefs.GetInt("crtNeurolonewsPopUp");
        GetComponent<SaveLastPosition>().SaveLastSceneAndPosition();
    }
    

    public void ChangeState(States newState)
    {
        Debug.Log("Current state =" + newState.ToString());
        if (transform.GetChild(0).gameObject.activeInHierarchy)
        {
            transform.GetChild(0).gameObject.SetActive(false);
            GetComponent<SpriteRenderer>().enabled = true;
        }
        switch (newState)
        {
            case States.IDLE:
                //activateAll(true); <-- Si on a un probl�me de state avec le Idle et les input, �a peut venir du fait que cette ligne est comment�e ^^
                //eventSystem.GetComponent<CrisisScript>().EndConvulsions();
                //eventSystem.GetComponent<TVScript>().shutDownTV();
                //eventSystem.GetComponent<SleepingScript>().wakeUp();
                changeButtonA(true);
                break;
            case States.GAMING:
                activateGameobject("Joystick", false);
                break;
            case States.TV:
                GetComponent<SpriteRenderer>().enabled = false;
                transform.GetChild(0).gameObject.SetActive(true);
                activateGameobject("Joystick", false);
                changeButtonA(false);
                //eventSystem.GetComponent<TVScript>().launchTV();
                break;
            case States.CRISIS:
                activateGameobject("Button-A", false);
                activateGameobject("Joystick", false);
                gameObject.GetComponent<CrisisScript>().StartConvulsions(gameObject, 5, 3f, true);
                break;
            case States.SLEEPING:
                activateGameobject("Joystick", false);
                changeButtonA(false);
                //eventSystem.GetComponent<SleepingScript>().goToSleep();
                break;
            case States.PAUSE:
                activateAll(false);
                break;
            case States.TALKING:
                changeButtonA(true);
                break;
            case States.BOARD:
                activateGameobject("Joystick", false);
                changeButtonA(false);
                break;
            case States.SHOPPING:
                activateGameobject("Joystick", false);
                changeButtonA(false);
                break;
            //Lorsqu'une activit� vient de se terminer
            case States.ACTIVITYFINISHED:
                activateAll(true);
                gameObject.GetComponent<CrisisScript>().EndConvulsions();
                changeButtonA(true);
                //eventSystem.GetComponent<TVScript>().shutDownTV();
                //eventSystem.GetComponent<SleepingScript>().wakeUp();
                break;

        }
        currentState = newState;
        Debug.Log("State is now " + currentState);
    }

    public void activateAll(bool active)
    {
        foreach(Transform go in uiElements.transform)
        {
            if(go.name!="HELP")
            go.gameObject.SetActive(active);
        }
    }

    public void activateGameobject(string name, bool active)
    {
        if(uiElements.transform.Find(name).gameObject.activeSelf!=active)
        {
            Debug.Log("Set " + name + " to " + active);
            uiElements.transform.Find(name).gameObject.SetActive(active);
            Debug.Log(uiElements.transform.Find(name).name + " is " + uiElements.transform.Find(name).gameObject.activeSelf + " now");
        }
    }

    public void setUiElements(GameObject uiElem)
    {
        uiElements = uiElem;
    }
    public void setParamElements(GameObject paramElem)
    {
        paramElements = paramElem;
    }
    public void setEventSystem(GameObject eventSys)
    {
        eventSystem = eventSys;
    }

    public void changeButtonA(bool green)
    {
        ColorBlock colors = uiElements.transform.Find("Button-A").gameObject.GetComponent<Button>().colors;
        if (green)
        {
            colors.normalColor = new Color32(132, 221, 93,255);
        }
        else
        {
            colors.normalColor = new Color32(236, 46, 48, 255);
        }
        uiElements.transform.Find("Button-A").gameObject.GetComponent<Button>().colors = colors;
    }
}
