using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;


public class PlayerMovement : MonoBehaviour
{
    [HideInInspector]
    public Rigidbody2D rigidBody;
    [HideInInspector]
    public Animator animator;
    public float moveSpeed;

    //Make instance of this script to be able reference from other scripts!
    public static PlayerController instance;

    [HideInInspector]
    public string areaTransitionName;
    private Vector3 boundary1;
    private Vector3 boundary2;

   // [HideInInspector]
    private bool canMove = true;
    private bool isMobile = true;

    private //Use Incontrol
    Renderer cachedRenderer = null;
    private Vector2 movement = Vector2.zero;
    private bool action1 = false, action2 = false;
    private bool actionCurrent = false;

    //Use Incontrol
    void Start()
    {
        cachedRenderer = GetComponent<Renderer>();
    }

    // Use this for initialization
    void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

   
    // Update is called once per frame
    void Update()
    {
        var inputDevice = InputManager.ActiveDevice;
        if (isMobile)
        {
            // Use last device which provided input.

            if (inputDevice != InputDevice.Null && inputDevice != TouchManager.Device)
            {
                TouchManager.ControlsEnabled = false;
            }

            buttonManagement(inputDevice);

            // Set target object material color based on which action is pressed.
            //cachedRenderer.material.color = GetColorFromActionButtons(inputDevice);

            if (canMove)
            {
                //rigidBody.velocity = new Vector2(Mathf.RoundToInt(CrossPlatformInputManager.GetAxis("Horizontal")), Mathf.RoundToInt(CrossPlatformInputManager.GetAxis("Vertical"))) * moveSpeed;
                rigidBody.velocity = new Vector2(Mathf.RoundToInt(inputDevice.Direction.X), inputDevice.Direction.Y) * moveSpeed;

                //Movement
                movement.x = inputDevice.Direction.X;
                movement.y = inputDevice.Direction.Y;
            }
            else
            {
                rigidBody.velocity = Vector2.zero;
                movement = Vector2.zero;
            }

            animator.SetFloat("moveX", rigidBody.velocity.x);
            animator.SetFloat("moveY", rigidBody.velocity.y);


            if (inputDevice.Direction.X == 1 || inputDevice.Direction.X == -1 || inputDevice.Direction.Y == 1 || inputDevice.Direction.X == -1)
            {
                
                if (canMove)
                {
                    animator.SetFloat("lastMoveX", inputDevice.Direction.X);
                    animator.SetFloat("lastMoveY", inputDevice.Direction.Y);

                    //Movement
                    movement.x = inputDevice.Direction.X;
                    movement.y = inputDevice.Direction.Y;
                }
            }


        }
    }

    void FixedUpdate()
    {
        rigidBody.MovePosition(rigidBody.position + movement * moveSpeed * Time.fixedDeltaTime);
    }

    static void buttonManagement(InputDevice inputDevice)
    {
        bool actionCurrently = false;

        var touchCount = TouchManager.TouchCount;

            Debug.Log("Start buttonManagement static method");
            //TentativeAction..
            if (inputDevice.Action1 && !actionCurrently)
            {
                actionCurrently = true;

                //Tmp
                Debug.Log("JZ_Action1 Pressed");
                GameObject.FindObjectOfType<StatusBarsManager>().GetComponent<StatusBarsManager>().SetEnergie(false, 20, 0.05f);
                GameObject.FindObjectOfType<StatusBarsManager>().GetComponent<StatusBarsManager>().SetFun(false, 20, 0.05f);

            }


            if (inputDevice.Action2 && !actionCurrently)
            {
                actionCurrently = true;

                //Tmp
                Debug.Log("JZ_Action2 Pressed");
                GameObject.FindObjectOfType<StatusBarsManager>().GetComponent<StatusBarsManager>().SetEnergie(true, -5, -0.8f);
                GameObject.FindObjectOfType<StatusBarsManager>().GetComponent<StatusBarsManager>().SetFun(true, -5, -0.8f);
            }

            actionCurrently = false;
 

       

    }

    //Method to set up the bounds which the player can not cross
    public void SetBounds(Vector3 bound1, Vector3 bound2)
    {
        boundary1 = bound1 + new Vector3(.5f, 1f, 0f);
        boundary2 = bound2 + new Vector3(-.5f, -1f, 0f);
    }

    static Color GetColorFromActionButtons(InputDevice inputDevice)
    {
        if (inputDevice.Action1)
        {
            return Color.green;
        }

        if (inputDevice.Action2)
        {
            return Color.red;
        }

        return Color.white;
    }

    //void OnGUI()
    //{
    //    var y = 10.0f;

    //    var touchCount = TouchManager.TouchCount;
    //    for (var i = 0; i < touchCount; i++)
    //    {
    //        var touch = TouchManager.GetTouch(i);
    //        var text = "" + i + ": fingerId = " + touch.fingerId;
    //        text = text + ", phase = " + touch.phase;
    //        text = text + ", startPosition = " + touch.startPosition;
    //        text = text + ", position = " + touch.position;

    //        if (touch.IsMouse)
    //        {
    //            text = text + ", mouseButton = " + touch.mouseButton;
    //        }

    //        GUI.Label(new Rect(10, y, Screen.width, y + 15.0f), text);
    //        y += 20.0f;
    //    }
    //}

}
