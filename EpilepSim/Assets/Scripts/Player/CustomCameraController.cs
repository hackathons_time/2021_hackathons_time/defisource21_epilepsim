﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;


public class CustomCameraController : MonoBehaviour
{

    [HideInInspector]
    public Transform player = default;
    public bool NeedFollowingPlayer = default;
    [Tooltip("/!\\ Attention la caméra suit les globales position, les parents du tilemap doivent être à 0,0,0")]
    public Tilemap tilemap = default;
    private Vector3 boundary1 = default;
    private Vector3 boundary2 = default;

    private float halfHeight = default;
    private float halfWidth = default;


    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerState>().transform;

        halfHeight = Camera.main.orthographicSize;
        halfWidth = halfHeight * Camera.main.aspect;

        tilemap.CompressBounds(); //compress tilemap to delete empty spacing tile
        boundary1 = tilemap.localBounds.min + new Vector3(halfWidth, halfHeight, 0f);
        boundary2 = tilemap.localBounds.max + new Vector3(-halfWidth, -halfHeight, 0f);

        GetComponent<Camera>().orthographic = false;
        if (GameObject.FindObjectOfType<MainLoadManger>().GetComponent<MainLoadManger>().IsCurrentSceneOutDoor())
        {
            GetComponent<Camera>().orthographic = true;
            NeedFollowingPlayer = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (NeedFollowingPlayer)
        {
            transform.position = new Vector3(player.position.x, player.position.y, transform.position.z);
            //keep the camera inside the bounds
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, boundary1.x, boundary2.x), Mathf.Clamp(transform.position.y, boundary1.y, boundary2.y), transform.position.z);
        }
    }
}
