using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class DevSam_PlayerControl : MonoBehaviour
{
    [HideInInspector]
    public Rigidbody2D rigidBody;
    [HideInInspector]
    public Animator animator;
    public float moveSpeed;

    //Make instance of this script to be able reference from other scripts!
    public static PlayerController instance;

    [HideInInspector]
    public string areaTransitionName;
    private Vector3 boundary1;
    private Vector3 boundary2;

    [HideInInspector]
    public bool canMove = true;

    //Use Incontrol
    Renderer cachedRenderer;
    //Use Incontrol
    void Start()
    {
        cachedRenderer = GetComponent<Renderer>();
    }

    // Use this for initialization
    void Awake()
    {

        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

        //if (instance == null)
        //{
        //    instance = this;
        //}
        //else
        //{
        //    if (instance != this)
        //    {
        //        Destroy(gameObject);
        //    }
        //}

        DontDestroyOnLoad(gameObject);
    }

    ///*
    // MOBILE INPUT
    // Uncomment this complete Update() function to enable mobile controls. But comment out the whole Update() function below this one.
    // Update is called once per frame
    void Update()
    {
        if (ControlManager.instance.mobile)
        {
            // Use last device which provided input.
            var inputDevice = InputManager.ActiveDevice;
            if (inputDevice != InputDevice.Null && inputDevice != TouchManager.Device)
            {
                TouchManager.ControlsEnabled = false;
            }
            // Set target object material color based on which action is pressed.
            cachedRenderer.material.color = GetColorFromActionButtons(inputDevice);

            if (canMove)
            {
                //rigidBody.velocity = new Vector2(Mathf.RoundToInt(CrossPlatformInputManager.GetAxis("Horizontal")), Mathf.RoundToInt(CrossPlatformInputManager.GetAxis("Vertical"))) * moveSpeed;
                rigidBody.velocity = new Vector2(Mathf.RoundToInt(inputDevice.Direction.X), inputDevice.Direction.Y) * moveSpeed;
            }
            else
            {
                rigidBody.velocity = Vector2.zero;

            }

            animator.SetFloat("moveX", rigidBody.velocity.x);
            animator.SetFloat("moveY", rigidBody.velocity.y);

            //if (CrossPlatformInputManager.GetAxisRaw("Horizontal") == 1 || CrossPlatformInputManager.GetAxisRaw("Horizontal") == -1 || CrossPlatformInputManager.GetAxisRaw("Vertical") == 1 || CrossPlatformInputManager.GetAxisRaw("Vertical") == -1)
            //{
            //    if (canMove)
            //    {
            //        animator.SetFloat("lastMoveX", CrossPlatformInputManager.GetAxisRaw("Horizontal"));
            //        animator.SetFloat("lastMoveY", CrossPlatformInputManager.GetAxisRaw("Vertical"));
            //    }
            //}


            if (inputDevice.Direction.X == 1 || inputDevice.Direction.X == -1 || inputDevice.Direction.Y == 1 || inputDevice.Direction.X == -1)
            {
                if (canMove)
                {
                    animator.SetFloat("lastMoveX", inputDevice.Direction.X);
                    animator.SetFloat("lastMoveY", inputDevice.Direction.Y);
                }
            }
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, boundary1.x, boundary2.x), Mathf.Clamp(transform.position.y, boundary1.y, boundary2.y), transform.position.z);
        }

        if (!ControlManager.instance.mobile)
        {
            if (canMove)
            {
                rigidBody.velocity = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
                rigidBody.velocity = rigidBody.velocity.normalized * moveSpeed;
            }
            else
            {
                rigidBody.velocity = Vector2.zero;

            }

            animator.SetFloat("moveX", rigidBody.velocity.x);
            animator.SetFloat("moveY", rigidBody.velocity.y);

            if (Input.GetAxisRaw("Horizontal") == 1 || Input.GetAxisRaw("Horizontal") == -1 || Input.GetAxisRaw("Vertical") == 1 || Input.GetAxisRaw("Vertical") == -1)
            {
                if (canMove)
                {
                    animator.SetFloat("lastMoveX", Input.GetAxisRaw("Horizontal"));
                    animator.SetFloat("lastMoveY", Input.GetAxisRaw("Vertical"));
                }
            }

            //This calculates the bounds and doesn't let the player go beyond the defined bounds
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, boundary1.x, boundary2.x), Mathf.Clamp(transform.position.y, boundary1.y, boundary2.y), transform.position.z);
        }

    }

    //Method to set up the bounds which the player can not cross
    public void SetBounds(Vector3 bound1, Vector3 bound2)
    {
        boundary1 = bound1 + new Vector3(.5f, 1f, 0f);
        boundary2 = bound2 + new Vector3(-.5f, -1f, 0f);
    }

    static Color GetColorFromActionButtons(InputDevice inputDevice)
    {
        if (inputDevice.Action1)
        {
            return Color.green;
        }

        if (inputDevice.Action2)
        {
            return Color.red;
        }

        if (inputDevice.Action3)
        {
            return Color.blue;
        }

        if (inputDevice.Action4)
        {
            return Color.yellow;
        }

        return Color.white;
    }

    void OnGUI()
    {
        var y = 10.0f;

        var touchCount = TouchManager.TouchCount;
        for (var i = 0; i < touchCount; i++)
        {
            var touch = TouchManager.GetTouch(i);
            var text = "" + i + ": fingerId = " + touch.fingerId;
            text = text + ", phase = " + touch.phase;
            text = text + ", startPosition = " + touch.startPosition;
            text = text + ", position = " + touch.position;

            if (touch.IsMouse)
            {
                text = text + ", mouseButton = " + touch.mouseButton;
            }

            GUI.Label(new Rect(10, y, Screen.width, y + 15.0f), text);
            y += 20.0f;
        }
    }
}
