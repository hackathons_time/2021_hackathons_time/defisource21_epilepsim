using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class InputsScript : MonoBehaviour
{
    public float speed;

    Rigidbody2D rb;

    Animator animator;

    
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        animator.enabled = true;
    }


    // Update is called once per frame
    void Update()
    {
        float movX = SimpleInput.GetAxisRaw("Horizontal");
        float movY = SimpleInput.GetAxisRaw("Vertical");

        rb.velocity = new Vector2(Mathf.RoundToInt(movX), Mathf.RoundToInt(movY)) * speed;

        animator.SetFloat("moveX", rb.velocity.x);
        animator.SetFloat("moveY", rb.velocity.y);

        if (CrossPlatformInputManager.GetAxisRaw("Horizontal") == 1 || CrossPlatformInputManager.GetAxisRaw("Horizontal") == -1 || CrossPlatformInputManager.GetAxisRaw("Vertical") == 1 || CrossPlatformInputManager.GetAxisRaw("Vertical") == -1)
        {
                animator.SetFloat("lastMoveX", CrossPlatformInputManager.GetAxisRaw("Horizontal"));
                animator.SetFloat("lastMoveY", CrossPlatformInputManager.GetAxisRaw("Vertical"));
        }

        if (Input.GetAxisRaw("Horizontal") == 1 || Input.GetAxisRaw("Horizontal") == -1 || Input.GetAxisRaw("Vertical") == 1 || Input.GetAxisRaw("Vertical") == -1)
        {
                animator.SetFloat("lastMoveX", Input.GetAxisRaw("Horizontal"));
                animator.SetFloat("lastMoveY", Input.GetAxisRaw("Vertical"));
        }

        if (movX == 1 || movX == -1 || movY == 1 || movY == -1)
        {
            animator.SetFloat("lastMoveX", SimpleInput.GetAxisRaw("Horizontal"));
            animator.SetFloat("lastMoveY", SimpleInput.GetAxisRaw("Vertical"));
        }
    }
}
