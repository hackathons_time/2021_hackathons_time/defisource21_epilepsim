using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttributesScript : MonoBehaviour
{
    private bool isBusy = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool getIsBusy()
    {
        return isBusy;
    }

    public void setIsBusy(bool busy)
    {
        isBusy = busy;
    }

}
