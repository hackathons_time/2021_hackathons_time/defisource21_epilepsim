using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveLastPosition : MonoBehaviour
{

    Vector3 currentPosition;

    public void FixedUpdate()
    {
        currentPosition = transform.localPosition;
    }


    public void SaveLastSceneAndPosition()
    {
        //Get name of courante scene and Save it
        PlayerPrefs.SetString("LastSceneName", SceneManager.GetActiveScene().name);

        //Get player position into string
        string lastPosition =
            currentPosition.x + ";" +
            currentPosition.y + ";" +
            currentPosition.z;

        //Save player position
        PlayerPrefs.SetString("LastPlayerPosition", lastPosition);
        //Debug.Log("SLP_Diff transform  :" + transform.position + " || vs locapostion to :" + transform.localPosition );
        //Debug.Log("SLP_ SAVE Player transform to save :" + currentPosition + " || convert to :" + lastPosition + " || Save as:" + PlayerPrefs.GetString("LastPlayerPosition"));
    }

    /// <summary>
    /// Convert lastPositionPlayer string into vector3,then return it
    /// </summary>
    /// <param name="lastPositionPlayer">this is a PlayerPrefs data "LastPlayerPosition"</param>
    /// <returns></returns>
    public Vector3 LastPostionPlayer(string lastPositionPlayer)
    {
  
        string[] position = lastPositionPlayer.Split(';');
        Debug.Log("SLP_RESTORE String convert to :" + lastPositionPlayer + " || Player transform to save :" + new Vector3(float.Parse(position[0]), float.Parse(position[1]), float.Parse(position[2])));
        return new Vector3(float.Parse(position[0]), float.Parse(position[1]), float.Parse(position[2]));
    }
}
