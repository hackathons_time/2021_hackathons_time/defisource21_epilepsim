using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardsManager : MonoBehaviour
{
    [SerializeField] private Transform Categories;
    [SerializeField] private Transform SubCategories;
    [SerializeField] private Transform cards;
    [SerializeField] private Transform BackSubCategories;
    [SerializeField] private Transform Backcards;

    public void Start()
    {
        ShowCategorie(Categories.gameObject);
    }

    //Show Sub
    public void ShowSubCatgories(GameObject Subcategorie)
    {
        foreach (Transform child in Categories.transform)
        {
            child.gameObject.SetActive(false);
        }

        foreach (Transform child in SubCategories.transform)
        {
            child.gameObject.SetActive(false);
        }
        foreach (Transform child in cards.transform)
        {
            child.gameObject.SetActive(false);
        }

        BackSubCategories.gameObject.SetActive(true);
        Subcategorie.SetActive(true);
    }

    public void ShowCard(GameObject card)
    {

        foreach (Transform child in SubCategories.transform)
        {
            child.gameObject.SetActive(false);
        }

        foreach (Transform child in cards.transform)
        {
            child.gameObject.SetActive(false);
        }


        Backcards.gameObject.SetActive(true);
        card.SetActive(true);
    }

    public void ShowCategorie(GameObject categorie)
    {
       

        foreach (Transform child in SubCategories.transform)
        {
            child.gameObject.SetActive(false);
        }

        foreach (Transform child in cards.transform)
        {
            child.gameObject.SetActive(false);
        }

        foreach (Transform child in Categories.transform)
        {
            child.gameObject.SetActive(true);
        }
    }
}
