using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YesNoScript : MonoBehaviour
{
    [SerializeField] GameObject yesNoPanel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClosePanel()
    {
        yesNoPanel.SetActive(false);
    }
}
