using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StatusBarsManager : MonoBehaviour
{

    [SerializeField] private float energyQuantity = 0;
    [SerializeField] private float funQuantity = 0;
    private float qtyByCycleF = 0.05f;
    private float qtyByCycleE = 0.05f;
    [SerializeField] private Gradient gradient = null;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject helpButton;
    public int helpButtonLimit;

    private Image energyBar = null;
    private Image funBar = null;
    private Slider energySlider = null;
    private float energyCrt = 0;
    private float energyDamage = 0;
    private Slider funSlider = null;
    private float funCrt = 0;
    private float funDamage = 0;
    private bool updatingEnergy = false,
                    updatingFun = false,
                    makeDamageE = false,
                    makeDamageF = false,
                    statusUpdating = false,
                    simpleAdd = false;
                    

    private Image smileyAnchor = null;
    public List<Sprite> smileys = null;

    [SerializeField] private TextMeshProUGUI moneyBag;



    public int indexActivity = -1;

    void Start()
    {
        energySlider = transform.GetChild(0).GetComponent<Slider>();
        energyBar = energySlider.transform.Find("Energy").GetComponent<Image>();
        funSlider = transform.GetChild(1).GetComponent<Slider>();
        funBar = funSlider.transform.Find("Fun").GetComponent<Image>();
        //Check if Datas exist

        if (PlayerPrefs.HasKey("Energy"))
        {
            Debug.Log("JZ_PlayerPref Energy have Key");
            energyQuantity = PlayerPrefs.GetFloat("Energy");
            energySlider.value = energyQuantity / 100f;
        }
        else
        {
            Debug.Log("JZ_PlayerPref Energy create New Key");
            energyQuantity =50;
            PlayerPrefs.SetFloat("Energy", energyQuantity);
            energySlider.value = energyQuantity / 100f;
        }

        if (PlayerPrefs.HasKey("Fun"))
        {
            Debug.Log("JZ_PlayerPref Energy have Key");
            funQuantity = PlayerPrefs.GetFloat("Fun");
            funSlider.value = funQuantity / 100f;
        }
        else
        {
            Debug.Log("JZ Energy create New Key");
            funQuantity = 50;
            PlayerPrefs.SetFloat("Fun", funQuantity);
            funSlider.value = funQuantity / 100f;
        }

        if (PlayerPrefs.HasKey("Money"))
        {
            Debug.Log("JZ_PlayerPref Money have Key");
            moneyBag.text = PlayerPrefs.GetFloat("Money") + "Pi�ces";
        }

        
    
        Debug.Log("JZ_EnergyValue:"+ energySlider.value + "Energy: "+ energyQuantity +"/100="+ energyQuantity / 100f);
        Debug.Log("JZ_FunValue:" + funSlider.value + "Energy: " + funQuantity + "/100=" + funQuantity / 100f);


        //energySlider.value = energyQuantity / 100f;
        //funSlider.value = funQuantity / 100f;
        smileyAnchor = funSlider.transform.Find("SmileyAnchor").GetComponent<Image>();


        energyCrt = energyQuantity;
        energyBar.color = gradient.Evaluate(energySlider.normalizedValue);

        //Max Fun
        funCrt = funQuantity;
        //funBar.color = gradient.Evaluate(1f);
        funBar.color = gradient.Evaluate(funSlider.normalizedValue);



        UpdateSmileyAnchors();
    }

    //Call To Save Energy and Fun
    public void SaveSatusbar()
    {
        Debug.Log("JZ_PlayerPrefs Save Energy: " + energyQuantity + "/" + PlayerPrefs.GetFloat("Energy") + " �� Save Fun: " + funQuantity + " / " + PlayerPrefs.GetFloat("Energy"));
        PlayerPrefs.SetFloat("Energy", energyQuantity);
        PlayerPrefs.SetFloat("Fun", funQuantity);
        
    }

    // Update is called once per frame
    void Update()
    {
        //Function to debug
        testKeyboard();

        //Update Energy
        if (updatingEnergy /*&& !statusUpdating*/)
        {
            //statusUpdating = true;
            if (makeDamageE)
            {
                if (energyQuantity - energyCrt >= 0f && energyQuantity>0)
                {
                    energyQuantity += qtyByCycleE;
                    if(energyQuantity - energyCrt <= 0f)
                    {
                        updatingEnergy = false;
                    }
                    energySlider.value = energyQuantity / 100f;
                    energyBar.color = gradient.Evaluate(energySlider.normalizedValue);
                }
                else
                {
                    updatingEnergy = false;
                    makeDamageE = false;
                }
            }
            else
            {
                if (energyQuantity - energyCrt <= 0f && energyQuantity < 100)
                {
                   
                    energyQuantity += qtyByCycleE;
                    if (energyQuantity >= 100f)
                    {
                        updatingEnergy = false;
                    }
                    energySlider.value = energyQuantity / 100f;
                    energyBar.color = gradient.Evaluate(energySlider.normalizedValue);
                }
                else
                {
                    updatingEnergy = false;
                }
            }
            //statusUpdating = false;

        }

        //Update Fun
        if (updatingFun)
        {
            //Check if dammages or heals
            if (makeDamageF)
            {
                if (funQuantity - funCrt >= 0f && funQuantity > 0)
                {
                    funQuantity += qtyByCycleF;
                    if (funQuantity <= 0f)
                    {
                        updatingFun = false;
                    }
                    funSlider.value = funQuantity / 100f;
                    funBar.color = gradient.Evaluate(funSlider.normalizedValue);

                    UpdateSmileyAnchors();
                }
                else
                {
                    updatingFun = false;
                    makeDamageF = false;
                }
            }
            else
            {
                if (funQuantity - funCrt <= 0f && funQuantity < 100)
                {
                    funQuantity += qtyByCycleF;
                    if (funQuantity >= 100f)
                    {
                        updatingFun = false;
                    }
                    funSlider.value = funQuantity / 100f;
                    funBar.color = gradient.Evaluate(funSlider.normalizedValue);

                    UpdateSmileyAnchors();
                }
                else
                {
                    updatingFun = false;
                }
            }
        }

       
    }

    

    private void FixedUpdate()
    {
        if (simpleAdd)
        {
            energySlider.value = energyQuantity / 100f;
            energyBar.color = gradient.Evaluate(energySlider.normalizedValue);

            funSlider.value = funQuantity / 100f;
            funBar.color = gradient.Evaluate(funSlider.normalizedValue);

            UpdateSmileyAnchors();
        }

        //Check energy for activate/desactivate helpButton
        if (player.GetComponent<PlayerState>().currentState.Equals(PlayerState.States.CRISIS) || energyQuantity > helpButtonLimit)
        {
            helpButton.SetActive(false);
        }
        else
        {
            funQuantity = 11;
            UpdateSmileyAnchors();
            helpButton.SetActive(true);
        }

        switch(player.GetComponent<PlayerState>().currentState)
        {
            case PlayerState.States.TV:
                Debug.Log("TV Test");
                SetFun(false, 1, .3f);
                SetEnergie(true, 1, 0.05f);
                break;
            case PlayerState.States.SLEEPING:
                Debug.Log("Bed Test");
                SetEnergie(false, 1, 0.5f);
                break;
            case PlayerState.States.GAMING:
                Debug.Log("Console Test");
                SetEnergie(true, 2, 0.01f);
                break;
            case PlayerState.States.CRISIS:
                Debug.Log("Crisis test");
                SetEnergie(false, 1, 0.02f);
                break;
        }

        if(!player.GetComponent<PlayerState>().currentState.Equals(PlayerState.States.CRISIS) && energyQuantity<=0)
        {
            player.transform.Rotate(0, 0, 90, 0);
            player.GetComponent<PlayerState>().ChangeState(PlayerState.States.CRISIS);
        }
    }

    public void UpdateSmileyAnchors()
    {
        if (funQuantity >= 100f)
        {
            smileyAnchor.sprite = smileys[0];
        }
        else if (funQuantity >= 75f)
        {
            smileyAnchor.sprite = smileys[1];
        }
        else if (funQuantity >= 50f)
        {
            smileyAnchor.sprite = smileys[2];
        }
        else if (funQuantity >= 25f)
        {
            smileyAnchor.sprite = smileys[3];
        }
        else if (funQuantity >= 10f)
        {
            smileyAnchor.sprite = smileys[4];
        }
    }

    public float getEnergyQuantity()
    {
        return energyQuantity;
    }

    public float getFunQuantity()
    {
        return funQuantity;
    }

    public void SetMoneyBag(float newValue)
    {
        moneyBag.text = newValue + "Pi�ces";
    }

    /// <summary>
    /// Set parameters to modidy energy. 
    /// </summary>
    /// <param name="damage"></param>
    /// <param name="quantityAffected">Use positives values to heals, and negative to dammages.</param>
    /// <param name="quantityAffectedByCycle">Use value near 0 to low animation and near 1 faster animation</param>
    public void SetEnergie(bool damage,float EnergyQuantityAffected, float EnergyQuantityAffectedByCycle)
    {
        Debug.Log("JZ_Set Energy WITH damamge " + damage + " by quantity affected : " + EnergyQuantityAffected + " Dameage by cylce " + EnergyQuantityAffectedByCycle);
        makeDamageE = damage;

        if (makeDamageE)
        {
            energyDamage = Mathf.Abs(EnergyQuantityAffected) * -1;
            qtyByCycleE = Mathf.Abs(EnergyQuantityAffectedByCycle) * -1;
        }
        else
        {
            energyDamage = Mathf.Abs(EnergyQuantityAffected);
            qtyByCycleE = EnergyQuantityAffectedByCycle;
        }
        
        energyCrt = energyQuantity + energyDamage;
        updatingEnergy = true;
    }

    /// <summary>
    /// Set parameters to modidy energy. 
    /// </summary>
    /// <param name="damage"></param>
    /// <param name="quantityAffected">Use positives values to heals, and negative to dammages.</param>
    /// <param name="quantityAffectedByCycle">Use value near 0 to low animation and near 1 faster animation</param>
    public void SetFun(bool damage, float funQuantityAffected, float funQuantityAffectedByCycle)
    {
        Debug.Log("JZ_Set fUN WITH damamge " + damage +" by quantity affected : " + funQuantityAffected + " Dameage by cylce " + funQuantityAffectedByCycle);
        makeDamageF = damage;
        if (makeDamageF)
        {
            funDamage = Mathf.Abs(funQuantityAffected) * -1;
            qtyByCycleF = Mathf.Abs(funQuantityAffectedByCycle) * -1;
        }
        else
        {
            funDamage = Mathf.Abs(funQuantityAffected);
            qtyByCycleF = funQuantityAffectedByCycle;
        }
        funCrt = funQuantity + funDamage;
        updatingFun = true;
    }

    public void SetSimpleEnergie(float quantity)
    {
        simpleAdd = true;
        energyQuantity += quantity;
       
    }

    public void SetSimplefun(float quantity)
    {
        simpleAdd = true;
        funQuantity += quantity;
    }

    public void StopSimpleAdd()
    {
        simpleAdd = true;
    }

    public void SetPlayer(GameObject pl)
    {
        player = pl;
    }

    public void SetHelpButton(GameObject hButton)
    {
        helpButton = hButton;
    }
    #region Debug Methods
    //Test Debug by button UI
    public void EnergieUp()
    {
        SetEnergie(false, 20, 0.05f);
        Debug.Log("JZ_Set Button Up Ernegy");
    }
    public void EnergieDown()
    {
        SetEnergie(true, -30, -0.05f);
        Debug.Log("JZ_Set Button Down Ernegy");
    }
    public void FunUp()
    {
        SetFun(false, 20, 0.05f);
        Debug.Log("JZ_Set Button Up Fun");
    }
    public void FunDown()
    {
        SetFun(true, -20, -0.05f);
        Debug.Log("JZ_Set Button Down Fun");
    }

    //Test Keyboard
    private void testKeyboard()
    {
        //Tout les inputs sont � changer !! Ils doivent �tre appel� par les items.
        if (Input.GetKeyDown(KeyCode.Q))
        {
            SetEnergie(true, -30, -0.8f);
            Debug.Log("JZ_Set Down Ernegy");
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            SetEnergie(false, 20, 0.05f);
            Debug.Log("JZ_Set Up Ernegy");
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SetFun(true, -30, -0.8f);
            Debug.Log("JZ_Set Down fun");
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            SetFun(false, 20, 0.05f);
            Debug.Log("JZ_Set Up fun");
        }

    }
    #endregion

}
