using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MedocBoxScript : MonoBehaviour
{
    [SerializeField] private int[] lundiMed = { 0, 0, 0, 0 };
    [SerializeField] private int[] mardiMed = { 0, 0, 0, 0 };
    [SerializeField] private int[] mercrediMed = { 0, 0, 0, 0 };
    [SerializeField] private int[] jeudiMed = { 0, 0, 0, 0 };
    [SerializeField] private int[] vendrediMed = { 0, 0, 0, 0 };
    [SerializeField] private int[] samediMed = { 0, 0, 0, 0 };
    [SerializeField] private int[] dimancheMed = { 0, 0, 0, 0 };

    [SerializeField] private Sprite fullMedoc;
    [SerializeField] private Sprite halfMedoc;

    [SerializeField] private GameObject medocBox;

    private ColorBlock defaultButtonColor;
    [SerializeField] private GameObject statusBar;

    public bool pillTaken = false;

    //QuestValidation
    [Header("Quest Elements")]
    [SerializeField] private int QuestIndexNumber;
    [SerializeField] private bool canStartQuest = false;
    public int currentQuest;
    public CustomQuestManager.QuestValidation currentValidation;
    [SerializeField] private CustomQuestManager customQuestManager;
    public bool validationProgress = false;

    // Start is called before the first frame update
    void Start()
    {
        defaultButtonColor = medocBox.transform.GetChild(0).transform.GetChild(0).GetComponent<Button>().colors;
        statusBar = GameObject.Find("StatusBar").gameObject;
        fillWeekBox();
        
        foreach(Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }

        customQuestManager = GameObject.FindObjectOfType<CustomQuestManager>().GetComponent<CustomQuestManager>();
    }

    public void FixedUpdate()
    {
        if (customQuestManager.GetCurrentQuestNumber() == currentQuest)
        {
            canStartQuest = true;
        }
        else
        {
            canStartQuest = false;
        }
    }

    public void fillWeekBox()
    {
        foreach (Transform day in medocBox.transform)
        {
            switch (day.name.ToLower())
            {
                case "lundi":
                    fillDayBox(lundiMed, getMomentsOfDay(day.gameObject));
                    break;

                case "mardi":
                    fillDayBox(mardiMed, getMomentsOfDay(day.gameObject));
                    break;

                case "mercredi":
                    fillDayBox(mercrediMed, getMomentsOfDay(day.gameObject));
                    break;

                case "jeudi":
                    fillDayBox(jeudiMed, getMomentsOfDay(day.gameObject));
                    break;

                case "vendredi":
                    fillDayBox(vendrediMed, getMomentsOfDay(day.gameObject));
                    break;

                case "samedi":
                    fillDayBox(samediMed, getMomentsOfDay(day.gameObject));
                    break;

                case "dimanche":
                    fillDayBox(dimancheMed, getMomentsOfDay(day.gameObject));
                    break;
            }
        }
    }

    /// <summary>
    /// Rempli un jour de la semaine
    /// </summary>
    /// <param name="dayPlan"> Planing d�fini par le joueur pour la journ�e en question</param>
    /// <param name="dayCases"> Liste des moments de la journ�e de la bo�te pour ce jour</param>
    public void fillDayBox(int[] dayPlan, List<GameObject> dayCases)
    {
        Debug.Log("SIZE: " + dayPlan.Length);

        for (int i = 0; i < dayPlan.Length; i++)
        {
            switch (dayPlan[i])
            {
                case 0:
                    dayCases[i].GetComponent<Image>().sprite = null;
                    break;

                case 1:
                    dayCases[i].GetComponent<Image>().sprite = halfMedoc;
                    break;

                case 2:
                    dayCases[i].GetComponent<Image>().sprite = fullMedoc;
                    break;

                default:
                    dayCases[i].GetComponent<Image>().sprite = null;
                    Debug.Log("INVALID ID OF MEDOC DETECTED");
                    break;
            }
        }
    }

    public List<GameObject> getMomentsOfDay(GameObject day)
    {
        List<GameObject> moments = new List<GameObject>();

        foreach (Transform child in day.transform)
        {
            moments.Add(child.gameObject);
        }

        return moments;
    }

    public void takeMedoc(GameObject dayCase)
    {
        int momentIndex = getMomentOfCase(dayCase);
        int dayIndex = getDayIndexOfCase(dayCase);

        switch (dayIndex)
        {
            case 0:
                lundiMed[momentIndex] = 0;
                dayCase.GetComponent<Image>().sprite = null;
                break;

            case 1:
                mardiMed[momentIndex] = 0;
                break;

            case 2:
                mercrediMed[momentIndex] = 0;
                break;

            case 3:
                jeudiMed[momentIndex] = 0;
                break;

            case 4:
                vendrediMed[momentIndex] = 0;
                break;

            case 5:
                samediMed[momentIndex] = 0;
                break;

            case 6:
                dimancheMed[momentIndex] = 0;
                break;
            default:
                Debug.Log("INVALID ID OF DAY");
                break;
        }

        dayCase.GetComponent<Button>().interactable = false;
        dayCase.GetComponent<Image>().sprite = null;
        dayCase.GetComponent<Button>().colors = defaultButtonColor;

        //Vital Gain
        Debug.Log("MEDOCS");
        statusBar.GetComponent<StatusBarsManager>().SetEnergie(false, 10, 10);

        if (canStartQuest)
        {
            pillTaken = true;
        }
    }

    private int getDayIndexOfCase(GameObject dayCase)
    {
        int dayIndex = Int32.Parse(dayCase.name.Substring(dayCase.name.Length - 1)) - 1;

        return dayIndex;
    }

    private int getMomentOfCase(GameObject dayCase)
    {
        string moment = dayCase.name.Split('_')[0];
        int momentIndex = 0;

        switch (dayCase.name.ToLower())
        {
            case "morning":
                momentIndex = 0;
                break;

            case "midday":
                momentIndex = 1;
                break;

            case "afternoon":
                momentIndex = 2;
                break;

            case "evening":
                momentIndex = 3;
                break;
            default:
                Debug.Log("INVALID MOMENT OF DAY");
                break;
        }

        return momentIndex;
    }

    public void OpenMedocBox()
    {
        customQuestManager.HideQuest();
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(true);
        }


    }

    public void CloseMedocBox()
    {
 
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
        if(canStartQuest && pillTaken)
        {
            ValideCurrentQuest();
        }
    }

    public void ValideCurrentQuest()
    {
        validationProgress = false;
        if (canStartQuest && customQuestManager.GetCurrentQuestNumber() == currentQuest)
        {
            Debug.Log("Quest validate");
            customQuestManager.ValideteQuest(currentValidation);

        }
        else
        {
            Debug.LogError("Quest Refused : " + canStartQuest + " // " + customQuestManager.GetCurrentQuestNumber() + " == " + currentQuest);
        }
    }

}
