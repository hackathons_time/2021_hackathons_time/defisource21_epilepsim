using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class Card
{
    public GameObject templateCard = default;
    public Sprite iconCard = default;
    public string titlte = default;
    public string text = default;
}

public class Tips : MonoBehaviour
{
    [SerializeField] private List<string> subCategories = default;
    [SerializeField] private List<GameObject> Cards = default;
    [SerializeField] private GameObject buttonToClone = default;
    [SerializeField] private GameObject AnchorsButton = default;
    [SerializeField] private GameObject AnchorsCards = default;
    [SerializeField] private GameObject CardTemplate = default;
    [SerializeField] private float speratorSub = default;

    private List<int> tmpIndexCard = new List<int>();
    int indxForeach = 0;
    void Start()
    {
        //Show Fisrt Button on scene
      
    }


    public void GenerateSubGategories(int indexList)
    {
        //Delete Subacategories
        if (AnchorsButton.transform.childCount != 0)
        {
            foreach (Transform child in AnchorsButton.transform)
            {
                Destroy(child.gameObject);
            }
        }
        //Delete Cards
        if (AnchorsCards.transform.childCount != 0)
        {
            foreach (Transform child in AnchorsCards.transform)
            {
                Destroy(child.gameObject);
            }
        }

        var indxForeach = 0;
        foreach (string child in subCategories)
        {
            InstantiateButton(indxForeach);
            indxForeach++;
        }

    }

    private void InstantiateButton(int index)
		{
			var SubClone = Instantiate(buttonToClone, new Vector2(AnchorsButton.transform.position.x, AnchorsButton.transform.position.y + (index * (buttonToClone.GetComponent<RectTransform>().rect.y + speratorSub))), Quaternion.identity);
            SubClone.transform.parent = AnchorsButton.transform;
            SubClone.transform.localScale = new Vector3(.12f, .2f, 1);
            SubClone.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = subCategories[index];
            SubClone.GetComponent<Button>().onClick.AddListener(delegate { GenerateCards(index); });
    }

    //Create Card by selection
    public void GenerateCards(int cardSelected)
    {
        Debug.Log("JZ_Nbre child AnchorsCards: " + AnchorsCards.transform.childCount, gameObject);

        //Delete All existing cards
        foreach (Transform child in AnchorsCards.transform)
        {
            Destroy(child.gameObject);
        }

        //check Cards
        for (int i = 0; i < Cards.Count; i++)
        {
            Debug.Log("JZ_Nbre Show Card : " + cardSelected + " and the current index is : " + i);
            //Create the good Card
            if (i == cardSelected)
            {
                var cardClone = Instantiate(Cards[i], new Vector2(AnchorsCards.transform.position.x, AnchorsCards.transform.position.y), Quaternion.identity);
                cardClone.transform.parent = AnchorsCards.transform;
                cardClone.transform.localScale = new Vector3(0.4f, 0.5f, 0.3f);
            }
        }
       
    }


}
