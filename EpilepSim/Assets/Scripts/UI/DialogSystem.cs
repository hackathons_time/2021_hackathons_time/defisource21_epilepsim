using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using InControl;
using UnityEngine.Events;

using static PlayerState;

public class DialogSystem : MonoBehaviour
{
    [Header("Action Button")]
    [SerializeField] private Button actionButton;

    [Header("Player")]
    [SerializeField] private GameObject playerInstance;

    [Header("Box")]
    [SerializeField] private GameObject dialogBox;
    [SerializeField] private GameObject questionBox;

    [Header("Personn")]
    [SerializeField] private string dialogPerson;
    [SerializeField] private Text dialogPersonBox;

    [Header("Text")]
    [SerializeField] [TextArea] private string dialogText;
    [SerializeField] private Text dialogTextBox;

    [Header("YesNoPanel")]
    [SerializeField] [TextArea] private string questionText;
    [SerializeField] private Text questionTextBox;

    [Header("Activity")]
    [SerializeField] private UnityEvent activityLaunch;
    [SerializeField] private UnityEvent activityClose;

    [Header("StateToChange")]
    [SerializeField] private int stateID;

    [Header("OtherActivitiesTriggers")]
    [SerializeField] private List<GameObject> activityTriggers;

    //CA, C'EST NOUVEAU (SAM)
    [Header("ActivityStartAndStop")]
    [SerializeField] private GameObject activityManager;
    [SerializeField] private UnityEvent startActivity;
    [SerializeField] private UnityEvent stopActivity;

    [Header("Effect sound")]
    private AudioMenu audioManager = default;

    //second Step
    public bool needSecondStep = false;

    // Start is called before the first frame update
    void Start()
    {
        //get ref
        audioManager = GameObject.FindObjectOfType<AudioMenu>();

        dialogBox.SetActive(false);
        questionBox.SetActive(false);

        //Take others activies from activitesManagers
        activityTriggers = activityManager.GetComponent<ActivityManager>().IsolateMyActivity(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        //var inputDevice = InputManager.ActiveDevice;
        if (playerInstance.GetComponent<AttributesScript>().getIsBusy())
        {
            //DisplayDialogBox(false, dialogBox);
            //DisplayDialogBox(true, questionBox);

            foreach (Transform child in questionBox.transform)
            {
                if(child.name=="Yes")
                {
                    Debug.Log("Button YES");
                    activityManager.GetComponent<ActivityManager>().UpdateActivitiesMethods();

                    child.GetComponent<Button>().onClick.AddListener(callChangeState);
                    child.GetComponent<Button>().onClick.AddListener(CloseQuestionBox);
                    child.GetComponent<Button>().onClick.AddListener(activityManager.GetComponent<ActivityManager>().startActivity.Invoke);
                   
                    actionButton.onClick.AddListener(closeActivity);
                    Debug.Log("DialogBox need secondeStep:" + needSecondStep);
                    if (needSecondStep)
                    {
                        Debug.LogWarning("DialogBox need secondeStep");
                        actionButton.onClick.AddListener(activityManager.GetComponent<ActivityManager>().activityLaunch.Invoke);
                    }
                    else
                    {
                        Debug.LogError("DialogBox need secondeStep:" + needSecondStep);
                        actionButton.onClick.AddListener(activityManager.GetComponent<ActivityManager>().stopActivity.Invoke);
                    }

                  
                }
                if(child.name == "No")
                {
                    Debug.Log("Button NO");
                    child.GetComponent<Button>().onClick.AddListener(CloseQuestionBox);
                }
            }
        }
    }

    public void DisplayDialogBox(bool active, GameObject box)
    {
        box.SetActive(active);   
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && !collision.GetComponent<AttributesScript>().getIsBusy() && playerInstance.GetComponent<PlayerState>().currentState== States.IDLE)
        {
            GameObject.FindObjectOfType<CustomQuestManager>().GetComponent<CustomQuestManager>().HideQuest();
            Debug.Log("Entr�e dans le trigger");
            DisplayDialogBox(true, dialogBox);

            dialogTextBox.text = dialogText;
            dialogPersonBox.text = dialogPerson;
            questionTextBox.text = questionText;

            playerInstance.gameObject.GetComponent<AttributesScript>().setIsBusy(true);

            actionButton.onClick.AddListener(CloseDialogBox);

            ActivateActivityTriggers(false);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player" && collision.GetComponent<AttributesScript>().getIsBusy())
        {
            DisplayDialogBox(false, dialogBox);
            collision.gameObject.GetComponent<AttributesScript>().setIsBusy(false);
            questionBox.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
            ActivateActivityTriggers(true);
            Debug.Log("State of player" + playerInstance.GetComponent<PlayerState>().currentState);
        }
        playerInstance.GetComponent<PlayerState>().ChangeState(PlayerState.States.IDLE);
        activityManager.GetComponent<ActivityManager>().eraseActivitiesMethods();

        /* Pr�vu en cas de crise!
         * Normalement, une activit� se quitte en appuyant sur A
         * Seulement, lors d'une crise, on va passer au state CRISIS, puis au state ACTIVITYFINISHED, et donc, rien n'�teindra la TV
         * Ces lignes servent donc � clore l'activit� une fois la crise termin�e, lorsque l'on sort du trigger de l'activit�
         * Si tu passes par l� et que tu as rien compris, Jeff, n'h�site pas � m'�crire xD
         */
        if (activityManager.GetComponent<ActivityManager>().stopActivity != null)
        {
            activityManager.GetComponent<ActivityManager>().stopActivity.Invoke();
        }
    }


    IEnumerator CloseQuestionBoxCoroutine()
    {
        DisplayDialogBox(false, questionBox);

        yield return new WaitForSeconds(5);

        if (playerInstance.GetComponent<AttributesScript>().getIsBusy())
        {
            DisplayDialogBox(true, questionBox);
        }
    }

    public void CloseQuestionBox()
    {
        //StartCoroutine(CloseQuestionBoxCoroutine());
        questionBox.SetActive(false);
    }

    public void CloseDialogBox()
    {
        Debug.Log("CloseDialogBox() launched");
        DisplayDialogBox(false, dialogBox);

        if (questionBox != null)
        {
            DisplayDialogBox(true, questionBox);
            actionButton.onClick.RemoveAllListeners();
        }
    }

    public void callChangeState()
    {
        playerInstance.GetComponent<PlayerState>().ChangeState((PlayerState.States)stateID);
    }

    public void closeActivity()
    {
        Debug.Log("Suppose to close the activity");
        playerInstance.GetComponent<PlayerState>().ChangeState(PlayerState.States.ACTIVITYFINISHED);
    }

    public void ActivateActivityTriggers(bool active)
    {
        foreach(GameObject T in activityTriggers)
        {
            T.SetActive(active);
        }
    }

    public UnityEvent getStartActivity()
    {
        //PlayEffectSound();
        return startActivity;
    }

    public UnityEvent getStopActivity()
    {
        return stopActivity;
    }

    //--------------------------------------------------------------------
    // Dialog & question box setters
    //--------------------------------------------------------------------
    public void setDialogBox(GameObject dialog)
    {
        dialogBox = dialog;
    }

    public void setQuestionBox(GameObject question)
    {
        questionBox = question;
    }

    //--------------------------------------------------------------------
    // Text box setters
    //--------------------------------------------------------------------
    public void setDialogTextBox(Text dialText)
    {
        dialogTextBox = dialText;
    }

    public void setPersonTextBox(Text persText)
    {
        dialogPersonBox = persText;
    }

    public void setQuestionTextBox(Text qText)
    {
        questionTextBox = qText;
    }

    //--------------------------------------------------------------------
    // Player & button setters
    //--------------------------------------------------------------------
    public GameObject getPlayer()
    {
        return playerInstance;
    }

    public void setPlayer(GameObject player)
    {
        playerInstance = player;
    }

    public void setActionButton(GameObject aButton)
    {
        actionButton = aButton.GetComponent<Button>();
    }


    //--------------------------------------------------------------------
    // Sound System
    //--------------------------------------------------------------------
    public void PlayEffectSound()
    {
        audioManager.PressDialog();
    }

}
