using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActivityManager : MonoBehaviour
{
    [Header("ActivityStartAndStop")]
    [SerializeField] public UnityEvent startActivity;
    [SerializeField] public UnityEvent stopActivity;

    [Header("ActivityLuanchAndClose")]
    [SerializeField] public UnityEvent activityLaunch;
    [SerializeField] public UnityEvent activityClose;

    [Header("ActivitiesParent")]
    [SerializeField] private GameObject activitiesParent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateActivitiesMethods()
    {
        for(int i = 0; i<activitiesParent.transform.childCount; i++)
        {
            if(activitiesParent.transform.GetChild(i).gameObject.activeSelf)
            {
                startActivity = activitiesParent.transform.GetChild(i).GetComponentInChildren<DialogSystem>().getStartActivity();
                stopActivity = activitiesParent.transform.GetChild(i).GetComponentInChildren<DialogSystem>().getStopActivity();
            }
        }

      
    }

    public void eraseActivitiesMethods()
    {
        startActivity = null;
        stopActivity = null;
    }

    /// <summary>
    /// Check all Activities and isolate mine for collision system. 
    /// </summary>
    /// <param name="currentGO">Use GO with DialogSystem. The function get automatically this firts parent </param>
    /// <returns></returns>
    public List<GameObject> IsolateMyActivity(GameObject currentTriggerGO)
    {
        List<GameObject> tmpList = new List<GameObject>();

        foreach (Transform child in activitiesParent.transform)
        {
            if (child.gameObject != currentTriggerGO.transform.parent.gameObject)
            {
                tmpList.Add(child.gameObject);
            }
        }

        return tmpList;
    }
}
