using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StoreActivity : MonoBehaviour
{
    [SerializeField] private GameObject storeGO;
    [SerializeField] private GameObject fullProductPanelGO;
    [SerializeField] private GameObject productManagerGO;
    [SerializeField] private TextMeshProUGUI monneyDisplay;
    [Tooltip("TEMPORAIRE. A SUPPRIMER UNE FOIS QUE LA MECANIQUE SERA FINALISEE")]
    [SerializeField] private List<GameObject> decoToActivate = new List<GameObject>();
    private string stringMonney;
    private float floatMonney;
    private GameObject player;
    private GameObject statusBar;

    //QuestValidation
    [Header("Quest Elements")]
    [SerializeField] private int QuestIndexNumber;
    [SerializeField] public bool canStartQuest = false;
    public int currentQuest;
    public CustomQuestManager.QuestValidation currentValidation;
    [SerializeField] private CustomQuestManager customQuestManager;
    public bool validationProgress = false;
    public bool stroreHasOpened = false;

    // Awake is called before Start
    void Awake()
    {
        customQuestManager = GameObject.FindObjectOfType<CustomQuestManager>().GetComponent<CustomQuestManager>();
    }


    // Start is called before the first frame update
    void Start()
    {
        statusBar = GameObject.FindObjectOfType<StatusBarsManager>().gameObject;
        player = gameObject.transform.GetChild(0).GetComponent<DialogSystem>().getPlayer();

        floatMonney = gameObject.transform.GetChild(0).GetComponent<DialogSystem>().getPlayer().GetComponent<MonneyScript>().getMonney();
        stringMonney = MonneyScript.formatPrice(floatMonney);

        fullProductPanelGO.GetComponent<FullProductPanel>().setPlayer(player);
        productManagerGO.GetComponent<ProductManager>().SetPlayer(player);

        for(int i = 0; i<decoToActivate.Count; ++i)
        {
            if (PlayerPrefs.HasKey("item" + i))
            {
                decoToActivate[PlayerPrefs.GetInt("item" + i)].SetActive(true);
            }
            else
            {
                decoToActivate[i].SetActive(false);
            }
        }




        //Disable the store
        CloseStore();
    }

    public void FixedUpdate()
    {
        if (customQuestManager.GetCurrentQuestNumber() == currentQuest)
        {
            canStartQuest = true;
        }
        else
        {
            canStartQuest = false;
        }
    }


    public void OpenStore()
    {
        statusBar.SetActive(false);
        storeGO.SetActive(true);
        updateDisplayMonney();

        if (canStartQuest)
        {
            stroreHasOpened = true;
        }
    }

    public void CloseStore()
    {
        storeGO.SetActive(false);
        statusBar.SetActive(true);
        statusBar.GetComponent<StatusBarsManager>().SetMoneyBag(gameObject.transform.GetChild(0).GetComponent<DialogSystem>().getPlayer().GetComponent<MonneyScript>().getMonney());

        if (canStartQuest && stroreHasOpened)
        {
            ValideCurrentQuest();
        }
    }

    public void buyProduct()
    {
        floatMonney -= FullProductPanel.currentProduct.price;
        gameObject.transform.GetChild(0).GetComponent<DialogSystem>().getPlayer().GetComponent<MonneyScript>().setMonney(floatMonney);
        updateDisplayMonney();

        switch(FullProductPanel.currentProduct.id)
        {
            case 0:
                decoToActivate[0].SetActive(true);
                decoToActivate[0].tag = "bought";
                PlayerPrefs.SetInt("item0",0);
                break;
            case 1:
                decoToActivate[1].SetActive(true);
                decoToActivate[1].tag = "bought";
                PlayerPrefs.SetInt("item1",1);
                break;
            case 2:
                decoToActivate[2].SetActive(true);
                decoToActivate[2].tag = "bought";
                PlayerPrefs.SetInt("item2",2);
                break;

            default:
                Debug.Log("Si tu es arriv� ici, c'est que tu es pass� par le Default du switch case pour activer les �l�ments de d�co. Globalement, c'est caca. Faut r�soudre �a, ptit con");
                break;
        }

        Debug.Log("Player monney: " + gameObject.transform.GetChild(0).GetComponent<DialogSystem>().getPlayer().GetComponent<MonneyScript>().getMonney());
        fullProductPanelGO.GetComponent<FullProductPanel>().updatePriceDisplay();
    }

    private void updateDisplayMonney()
    {
        updateStringMonney();
        monneyDisplay.text = stringMonney;
    }

    private void updateStringMonney()
    {
        floatMonney = gameObject.transform.GetChild(0).GetComponent<DialogSystem>().getPlayer().GetComponent<MonneyScript>().getMonney();
        stringMonney = MonneyScript.formatPrice(floatMonney);
    }

    public void ValideCurrentQuest()
    {
        validationProgress = false;
        if (canStartQuest && customQuestManager.GetCurrentQuestNumber() == currentQuest)
        {
            Debug.Log("Quest validate");
            customQuestManager.ValideteQuest(currentValidation);

        }
        else
        {
            Debug.LogError("Quest Refused : " + canStartQuest + " // " + customQuestManager.GetCurrentQuestNumber() + " == " + currentQuest);
        }
    }
}
