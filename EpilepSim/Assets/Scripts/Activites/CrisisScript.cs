using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrisisScript : MonoBehaviour
{
    bool shaking = false;

    private Animator playerAnimator;
    private float animSpeed;

    // Start is called before the first frame update
    void Start()
    {
        //convulsions(GameObjectToShake, 5, 3f, true);
        playerAnimator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region convulsion
    IEnumerator shakeGameObjectCOR(GameObject objectToShake, float totalShakeDuration, float decreasePoint)
    {
        if (decreasePoint >= totalShakeDuration)
        {
            Debug.LogError("decreasePoint must be less than totalShakeDuration...Exiting");
            yield break; //Exit!
        }

        //Get Original Pos and rot
        Transform objTransform = objectToShake.transform;
        Vector3 defaultPos = objTransform.position;
        Quaternion defaultRot = objTransform.rotation;

        float counter = 0f;

        //Shake Speed
        const float speed = 0.1f;

        //Angle Rotation(Optional)
        const float angleRot = 4;

        gameObject.GetComponent<Animator>().SetBool("isConvulsing", true);
        //Do the actual shaking
        while (counter < totalShakeDuration)
        {
            counter += Time.deltaTime;
            float decreaseSpeed = speed;
            float decreaseAngle = angleRot;

            animSpeed = (totalShakeDuration - counter) / totalShakeDuration;
            playerAnimator.speed = animSpeed;

            //Shake GameObject

            //Don't Translate the Z Axis if 2D Object
            Vector3 tempPos = defaultPos + UnityEngine.Random.insideUnitSphere * decreaseSpeed;
            tempPos.z = defaultPos.z;
            objTransform.position = tempPos;

            //Only Rotate the Z axis if 2D
            objTransform.rotation = defaultRot * Quaternion.AngleAxis(UnityEngine.Random.Range(-angleRot, angleRot), new Vector3(0f, 0f, 1f));

            yield return null;


            //Check if we have reached the decreasePoint then start decreasing  decreaseSpeed value
            if (counter >= decreasePoint)
            {
                //Debug.Log("Decreasing shake");

                //Reset counter to 0 
                counter = 0f;
                while (counter <= decreasePoint)
                {
                    counter += Time.deltaTime;
                    decreaseSpeed = Mathf.Lerp(speed, 0, counter / decreasePoint);
                    decreaseAngle = Mathf.Lerp(angleRot, 0, counter / decreasePoint);

                    //Debug.Log("Decrease Value: " + decreaseSpeed);

                    //Shake GameObject

                    //Don't Translate the Z Axis if 2D Object
                    Vector3 tempPosD = defaultPos + UnityEngine.Random.insideUnitSphere * decreaseSpeed;
                    tempPosD.z = defaultPos.z;
                    objTransform.position = tempPosD;

                    //Only Rotate the Z axis if 2D
                    objTransform.rotation = defaultRot * Quaternion.AngleAxis(UnityEngine.Random.Range(-decreaseAngle, decreaseAngle), new Vector3(0f, 0f, 1f));

                    yield return null;
                }

                //Break from the outer loop
                break;
            }
        }
        objTransform.position = defaultPos; //Reset to original postion
        objTransform.rotation = defaultRot;//Reset to original rotation

        shaking = false; //So that we can call this function next time
        Debug.Log("Done!");
        gameObject.GetComponent<PlayerState>().ChangeState(PlayerState.States.ACTIVITYFINISHED);
    }


    public void StartConvulsions(GameObject objectToShake, float shakeDuration, float decreasePoint, bool objectIs2D = false)
    {
        if (shaking)
        {
            return;
        }
        shaking = true;
        StartCoroutine(shakeGameObjectCOR(objectToShake, shakeDuration, decreasePoint));
        gameObject.transform.Rotate(0, 0, -90, 0);
        gameObject.GetComponent<Collider2D>().enabled = false;

        //Attention c'est en brut car si on fait une crise et qu'il ya pas de tV ca va buger
        if (GameObject.FindObjectOfType<TVScript>())
        {
            GameObject.FindObjectOfType<TVScript>().tvPlayed = false;
            GameObject.FindObjectOfType<TVScript>().canStartQuest = false;
        }
   
    }

    public void EndConvulsions()
    {
        StopAllCoroutines();
        gameObject.GetComponent<Animator>().SetBool("isConvulsing", false);
        playerAnimator.speed = 1;
        gameObject.transform.SetPositionAndRotation(gameObject.transform.position, new Quaternion(0, 0, 0, 0));
        gameObject.GetComponent<Collider2D>().enabled = true;
    }
    #endregion

    #region absence
    #endregion
}
