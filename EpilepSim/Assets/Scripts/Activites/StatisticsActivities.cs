using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatisticsActivities : MonoBehaviour
{

    [SerializeField] private GameObject boardPannel = default;
    [SerializeField] private List<GameObject> boardTabs = default;
    [SerializeField] private List<GameObject> boardBtns = default;
    [SerializeField] private GameObject neuroPopUpGO = default;

    private GameObject statusBar;

    //List of Manager
    BadgeManager badgeManager = default;
    TipsManager tipsManager = default;
    NeurloNewsManager neurloNewsManager = default;

    //QuestValidation
    [Header("Quest Elements")]
    [SerializeField] private int QuestIndexNumber;
    [SerializeField] public bool canStartQuest = false;
    public int currentQuest;
    public CustomQuestManager.QuestValidation currentValidation;
    [SerializeField] private CustomQuestManager customQuestManager;
    public bool validationProgress = false;
    public bool neuroTabOpened = false;

    // Awake is called before Start
    void Awake()
    {
        customQuestManager = GameObject.FindObjectOfType<CustomQuestManager>().GetComponent<CustomQuestManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //Get reference
        badgeManager = GameObject.FindObjectOfType<BadgeManager>().GetComponent<BadgeManager>();
        tipsManager = GameObject.FindObjectOfType<TipsManager>().GetComponent<TipsManager>();
        neurloNewsManager = GameObject.FindObjectOfType<NeurloNewsManager>().GetComponent<NeurloNewsManager>();
        statusBar = GameObject.FindObjectOfType<StatusBarsManager>().gameObject;

        //Update Visual
        ActiveActivitesPannel(false);
        neuroPopUpGO.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //For Debug
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("JZ_SpacePressed");
            GenreateNewNeuroloNews(neurloNewsManager.GetRandomNeurlonewsLocked());
        }
    }

    public void FixedUpdate()
    {
        if (customQuestManager.GetCurrentQuestNumber() == currentQuest)
        {
            canStartQuest = true;
        }
        else
        {
            canStartQuest = false;
        }
    }


    public void ActiveActivitesPannel(bool needActive)
    {
        statusBar.SetActive(!needActive);
        boardPannel.SetActive(needActive);
        //Validation QUete
        if (needActive == false)
        {
            if (canStartQuest && neuroTabOpened)
            {
                ValideCurrentQuest();
            }
        }
        ShowPanel(0);
    }

    /// <summary>
    /// Use it to hide all content of boardPannel and show index tabs choiced
    /// </summary>
    /// <param name="index">0-Badge/1->Neurolonews/2->Tips</param>
    public void ShowPanel(int index)
    {
        //Selected Tbs
        foreach(GameObject child in boardTabs)
        {
            child.gameObject.SetActive(false);
        }

        boardTabs[index].SetActive(true);

        if (index == 1)
        {
            GameObject.FindObjectOfType<NeurloNewsManager>().GetComponent<NeurloNewsManager>().OpenNeuroloNewsTab();

            if (canStartQuest)
            {
                neuroTabOpened = true;
            }
        }

        //Selected butns 
        foreach (GameObject child in boardBtns)
        {
            child.GetComponent<Button>().interactable = true;
        }

        boardBtns[index].GetComponent<Button>().interactable = false;
    }

    /*Lorsqu'un pop-up appara�t, ouvrir le board sur la bonne tab et cliquer sur le dernier �l�ment.
     */
    /// <summary>
    /// Fill template of Neurlonews Pop Up and Show it
    /// </summary>
    /// <param name="neuroPop"></param>
    public void GenreateNewNeuroloNews(NeurloNewsProfil neuroPop)
    {
        Debug.Log("JZ_POPUP New Neuro profil : " + neuroPop.id);
     
        // neuroPopUpGO.GetComponent<FillNeurloNews>().neurloNewsProfil = neuroPop;
        // neuroPop.isUnlocked = true;
     
        neuroPopUpGO.transform.GetChild(0).GetComponent<FillNeurloNews>().needUpdate = true;
        neuroPopUpGO.transform.GetChild(0).GetComponent<FillNeurloNews>().UpdateProfil(neuroPopUpGO.transform.GetChild(0).gameObject,neuroPop);
  
        neuroPopUpGO.SetActive(true);


        neurloNewsManager.NewUnlockedNeurolonews(neuroPop);
    }

    public void ClosePopUpPannel(GameObject go)
    {
        go.SetActive(false);
        GameObject.FindObjectOfType<NeurloNewsManager>().GetComponent<NeurloNewsManager>().isAdding = false;

       
    }

   public void ActivePopUpDisplay()
    {
        Debug.Log("JZ_ CAN POPUP And ActiveDisplay");
        GenreateNewNeuroloNews(neurloNewsManager.GetRandomNeurlonewsLocked());
    }

    public void ActivePopUpDisplay(NeurloNewsProfil newProfil)
    {
        Debug.Log("JZ_ CAN POPUP And ActiveDisplay");
        GenreateNewNeuroloNews(newProfil);
    }

    public void ValideCurrentQuest()
    {
        validationProgress = false;
        if (canStartQuest && customQuestManager.GetCurrentQuestNumber() == currentQuest)
        {
            Debug.Log("Quest validate");
            customQuestManager.ValideteQuest(currentValidation);

        }
        else
        {
            Debug.LogError("Quest Refused : " + canStartQuest + " // " + customQuestManager.GetCurrentQuestNumber() + " == " + currentQuest);
        }
    }
}
