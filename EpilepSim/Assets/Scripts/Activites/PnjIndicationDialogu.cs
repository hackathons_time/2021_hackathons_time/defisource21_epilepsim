using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PnjIndicationDialogu : MonoBehaviour
{
    /*Ouvrir la boite de dialogue
     * Mettre un texte si qu�te possible, si pas possible
     * Raccorder � une quete.
     * Est ce que la qu�te est possible alor lancer la q�ete sinon emp�cher.
     */
    [SerializeField] private GameObject DialogPannel;
    [SerializeField] private Text PnjTxtName;
    [SerializeField] private Text PnjTxtDialogue;

    [SerializeField] private bool talkingItself = false;
    [SerializeField] private string pnjName;
    [SerializeField] private string textQuest;
    [SerializeField] private string textAbort;

    [SerializeField] private int QuestIndexNumber;
    [SerializeField] private bool canStartQuest = false;

    //QuestValidation
    [Header("Quest Elements")]
    public int currentQuest;
    public CustomQuestManager.QuestValidation currentValidation;
    [SerializeField] private CustomQuestManager customQuestManager;
    public bool validationProgress = false;

    [Header("If is Sammy")]
    public bool isSammy = false;

    private void Awake()
    {
        //Get ref
        if (!talkingItself)
        {
            DialogPannel = GameObject.Find("DialogPNJBox");
        }
        else
        {
            DialogPannel = GameObject.Find("DialogReflexionBox");
        }

        PnjTxtName = DialogPannel.transform.GetChild(0).Find("TextName").GetComponent<Text>();
        PnjTxtDialogue = DialogPannel.transform.GetChild(1).Find("TextDialogue").GetComponent<Text>();
        customQuestManager = GameObject.FindObjectOfType<CustomQuestManager>().GetComponent<CustomQuestManager>();
    }

    void Start()
    {
        if (DialogPannel.activeInHierarchy)
        {
            DialogPannel.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        if (customQuestManager.GetCurrentQuestNumber() == currentQuest)
        {
            canStartQuest = true;
        }
        else
        {
            canStartQuest = false;
        }
    }


    public void StartDialogue()
    {
        if (!validationProgress)
        {
            PnjTxtName.text = pnjName;
            customQuestManager.HideQuest();
            if (canStartQuest)
            {
                PnjTxtDialogue.text = textQuest;
                //GameObject.FindObjectOfType<CustomQuestManager>().GetComponent<CustomQuestManager>().ChangeQuestGO(QuestIndexNumber);
               
                StartCoroutine(WaitToValidate(3F));
            }
            else
            {
                PnjTxtDialogue.text = textAbort;
            }

            DialogPannel.SetActive(true);
        }

    }

    public void ValideCurrentQuest()
    {
        validationProgress = false;
        if (canStartQuest && customQuestManager.GetCurrentQuestNumber() == currentQuest)
        {
            Debug.Log("Quest validate");
            customQuestManager.ValideteQuest(currentValidation);
            //GameObject.FindObjectOfType<DialogSystem>().needSecondStep = true;

        }
        else
        {
            Debug.LogError("Quest Refused : " + canStartQuest + " // " + customQuestManager.GetCurrentQuestNumber() + " == " + currentQuest);
        }
    }

    public void CLoseDialogueBoxPNJ()
    {
        if (!validationProgress)
        {
            DialogPannel.SetActive(false);
            StopAllCoroutines();
        }
    }

    private IEnumerator WaitToValidate(float waitTime)
    {
        validationProgress = true;
        yield return new WaitForSeconds(waitTime);

        DialogPannel.SetActive(false);
        ValideCurrentQuest();
        if (isSammy)
        {
            GetComponent<CheckCurrentState>().ShowNews();
        }
    }

 
}
