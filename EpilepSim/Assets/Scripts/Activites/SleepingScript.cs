using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SleepingScript : MonoBehaviour
{

    [SerializeField] private GameObject statusBarParent;
    [SerializeField] private GameObject playerParent;
    [SerializeField] private GameObject playerSleeping;
    [SerializeField] private float nightDuration;

    [SerializeField] private float energieByCycle = 5;
    [SerializeField] private float funByCycle = 1;

    private GameObject player;
    private GameObject statusBar;
   
    private bool nightmare;
    //QuestValidation
    [Header("Quest Elements")]
    [SerializeField] private int QuestIndexNumber;
    [SerializeField] public bool canStartQuest = false;
    public int currentQuest;
    public CustomQuestManager.QuestValidation currentValidation;
    [SerializeField] private CustomQuestManager customQuestManager;
    public bool validationProgress = false;
    public bool hasSleep = false;

    // Awake is called before Start
    void Awake()
    {
        customQuestManager = GameObject.FindObjectOfType<CustomQuestManager>().GetComponent<CustomQuestManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        player = playerParent.transform.GetChild(1).gameObject;
        statusBar = statusBarParent.transform.Find("StatusBar").gameObject;

        //------------------------------------------------------------

        playerSleeping.SetActive(false);
    }
    public void FixedUpdate()
    {
        if (customQuestManager.GetCurrentQuestNumber() == currentQuest)
        {
            canStartQuest = true;
        }
        else
        {
            canStartQuest = false;
        }
    }

    public void goToSleep()
    {
        customQuestManager.HideQuest();
        if (canStartQuest)
        {
            hasSleep = true;
        }

        player.SetActive(false);
        playerSleeping.SetActive(true);

        //nightmare = nightmareRandomizer();

        //StartCoroutine(increaseEnergy());

        if (nightmare)
        {
            //StartCoroutine(decreaseFun());
        }
        else
        {
            //StartCoroutine(increaseFun());
        }
    }

    public void wakeUp()
    {
        StopAllCoroutines();

        playerSleeping.SetActive(false);
        player.SetActive(true);

        Debug.Log("Wake-Up!");

        if (canStartQuest && hasSleep)
        {
            ValideCurrentQuest();
        }
    }


    private IEnumerator increaseFun()
    {
        while (true)
        {
            statusBar.GetComponent<StatusBarsManager>().SetSimplefun(funByCycle);
          yield return new WaitForSeconds(nightDuration);
            //statusBar.GetComponent<StatusBarsManager>().SetFun(false, 1, 1);
            statusBar.GetComponent<StatusBarsManager>().StopSimpleAdd();
        }
        yield break;
    }

    private IEnumerator decreaseFun()
    {
        while (true)
        {
            statusBar.GetComponent<StatusBarsManager>().SetSimpleEnergie(energieByCycle);
            yield return new WaitForSeconds(nightDuration);
            //statusBar.GetComponent<StatusBarsManager>().SetFun(true, 1, 1);
            statusBar.GetComponent<StatusBarsManager>().StopSimpleAdd();
        }
        yield break;
    }

    private IEnumerator increaseEnergy()
    {
        Debug.Log("Fait dodo");
        while (true)
        {
            yield return new WaitForSeconds(nightDuration);
            statusBar.GetComponent<StatusBarsManager>().SetEnergie(false, 10, 10);
        }
        //wakeUp();

        yield break;
    }

    public bool nightmareRandomizer()
    {
        return  (UnityEngine.Random.value > 0.5f);
    }

    public void ValideCurrentQuest()
    {
        validationProgress = false;
        if (canStartQuest && customQuestManager.GetCurrentQuestNumber() == currentQuest)
        {
            Debug.Log("Quest validate");
            customQuestManager.ValideteQuest(currentValidation);

        }
        else
        {
            Debug.LogError("Quest Refused : " + canStartQuest + " // " + customQuestManager.GetCurrentQuestNumber() + " == " + currentQuest);
        }
    }
}
