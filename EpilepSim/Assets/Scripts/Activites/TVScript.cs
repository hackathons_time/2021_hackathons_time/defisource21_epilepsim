using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class TVScript : MonoBehaviour
{
    [SerializeField] private VideoClip tvShow;
    [SerializeField] private VideoPlayer videoPlayer;
    [SerializeField] private GameObject statusBarParent;
    [SerializeField] private GameObject statusBar;
    [SerializeField] private GameObject player;

    
    //QuestValidation
    [Header("Quest Elements")]
    [SerializeField] private int QuestIndexNumber;
    [SerializeField] public bool canStartQuest = false;
    public int currentQuest;
    public CustomQuestManager.QuestValidation currentValidation;
    [SerializeField] private CustomQuestManager customQuestManager;
    public bool validationProgress = false;
    public bool tvPlayed = false;


    // Awake is called before Start
    void Awake()
    {
        customQuestManager = GameObject.FindObjectOfType<CustomQuestManager>().GetComponent<CustomQuestManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        statusBar = statusBarParent.transform.GetChild(2).gameObject;

        //------------------------------------------------------------

        videoPlayer.clip = null;
        videoPlayer.gameObject.SetActive(false);

        //------------------------------------------------------------
        //player = GameObject.FindObjectOfType<PlayerState>().gameObject;
        //player.transform.GetChild(0).gameObject.SetActive(false);
    }

    public void FixedUpdate()
    {
        if (customQuestManager.GetCurrentQuestNumber() == currentQuest)
        {
            canStartQuest = true;
        }
        else
        {
            canStartQuest = false;
        }
    }


    public void launchTV()
    {
        customQuestManager.HideQuest();
        if (canStartQuest)
        {
            tvPlayed = true;
        }
        videoPlayer.clip = tvShow;
        videoPlayer.gameObject.SetActive(true);
        //StartCoroutine(increaseFun());
        //StartCoroutine(decreaseEnergyTV());
        //player.GetComponent<SpriteRenderer>().enabled = false;
        //player.transform.GetChild(0).gameObject.SetActive(true);
    }

    public void shutDownTV()
    {
        //StopAllCoroutines();
        videoPlayer.clip = null;
        videoPlayer.gameObject.SetActive(false);
        ////
        //player.transform.GetChild(0).gameObject.SetActive(false);
        //player.GetComponent<SpriteRenderer>().enabled = true;

        if (canStartQuest && tvPlayed)
        {
            ValideCurrentQuest();
        }
    }

    public void ValideCurrentQuest()
    {
        validationProgress = false;
        if (canStartQuest && customQuestManager.GetCurrentQuestNumber() == currentQuest)
        {
            Debug.Log("Quest validate");
            customQuestManager.ValideteQuest(currentValidation);

        }
        else
        {
            Debug.LogError("Quest Refused : " + canStartQuest + " // " + customQuestManager.GetCurrentQuestNumber() + " == " + currentQuest);
        }
    }

    //private IEnumerator increaseFun()
    //{
    //     while ((int)statusBar.GetComponent<StatusBarsManager>().getFunQuantity()!=100)
    //     {
    //        yield return new WaitForSeconds(1f);
    //        statusBar.GetComponent<StatusBarsManager>().SetFun(false, 1, 1);
    //     }
    //    yield break;
    //}

    //private IEnumerator decreaseEnergyTV()
    //{
    //    Debug.Log("Regarde la TV");
    //    while ((int)statusBar.GetComponent<StatusBarsManager>().getEnergyQuantity() != 0)
    //    {
    //        yield return new WaitForSeconds(1f);
    //        statusBar.GetComponent<StatusBarsManager>().SetEnergie(true, 1, 1);
    //    }
    //    yield break;
    //}
}
