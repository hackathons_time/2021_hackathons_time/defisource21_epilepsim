using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System.Linq;
using Unity.Jobs;
using static SaveNeuroData;

//[RequireComponent(typeof(NeurloNewsManager))]
public class SaveNeuroNewsManager : MonoBehaviour, ISavableNeuro
{
    [SerializeField] private Transform container;
    [SerializeField] private GameObject prefabToClone;
    [SerializeField] public List<GameObject> neurolonewListCrt = new List<GameObject>();


    //Serializefile to Debug
    [SerializeField] private string data = null;
    [SerializeField] private List<string> groupe = null;

    //Data
    [SerializeField] private string neuroData = null;

    //Neuro
    [SerializeField] private Object[] neurolonewList = default;

    // Start is called before the first frame update
    void Awake()
    {
        container = GetComponent<NeurloNewsManager>().GetContainerNeurolonews();
        //prefabToClone = GetComponent<NeurloNewsManager>().GetFullNeurolonewPanelGO();
    }

    void Start()
    {
        data = null;
        groupe = null;

        //Get Ref
        neurolonewList = Resources.LoadAll("Neurolonews", typeof(GameObject));
        

    }

    /*//Buttons
     */
    //Bouton Rest Scene
    public void ResetScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    //Delete
    public void DeleteContainerContent()
    {
        Debug.Log("Delete:"+ container);
        
        if (container.childCount > 0)
        {
            foreach (Transform child in container.transform)
            {
                Debug.Log("Deleting:" + child);
                Destroy(child.gameObject);
            }
          
        }
    }

    //Add Object
    public void AddContainerContent()
    {

        int randomIndex = Random.Range(0, neurolonewList.Count() - 1);
        Debug.Log("randomIndex tmp: " + randomIndex);
        GameObject clone = Instantiate(prefabToClone, container.position, Quaternion.identity);
        clone.name = "Clone_" + container.childCount;
        //clone.AddComponent<FillNeurloNews>();
        Debug.Log("randomIndex tmp: " + randomIndex + " // " + clone.name);
        GameObject tmpProfil = neurolonewList[randomIndex] as GameObject;
        foreach (GameObject g in neurolonewList)
        {
            //Badge(g);
            if (g.GetComponent<FillNeurloNews>().neurloNewsProfil == tmpProfil.GetComponent<FillNeurloNews>().neurloNewsProfil)
            {
                clone.GetComponent<FillNeurloNews>().neurloNewsProfil = g.GetComponent<FillNeurloNews>().neurloNewsProfil;
            }
        }
        clone.transform.SetParent(container);
        clone.transform.localScale = Vector3.one;

    }

    public void SaveContainerData()
    {
        Debug.Log("saving, container is : " + container);
        string tmp = null;
        int position = 0;
        foreach (Transform child in container)
        {
            //SaveSystem.SavePlayer(child.gameObject.name);
            tmp = tmp + position + ":" + child.GetComponent<FillNeurloNews>().neurloNewsProfil + ";";
            position++;
            Debug.Log("Child:" + child.name);
        }
        Debug.Log("data:" + tmp);
        data = tmp;

        //
        SaveNeuro();
    }

    //Load
    public void LoadContainerData()
    {
        DeleteContainerContent();
        neurolonewListCrt.Clear();


        LoadNeuro();

        Debug.Log("Loading");

        groupe = data.Split(';').ToList<string>();
      

        foreach (string child in groupe)
        {
            Debug.Log("Loading - search Groupe");
            GameObject clone = Instantiate(prefabToClone, container.position, Quaternion.identity);
            clone.SetActive(true);
            foreach (GameObject g in neurolonewList)
            {
                Debug.Log("Loading - search profil");
                //Badge(g);
                if (g.GetComponent<FillNeurloNews>().neurloNewsProfil.ToString() == child.Substring(child.LastIndexOf(':') + 1))
                {
                    Debug.Log("Loading - profil found: "+ g.GetComponent<FillNeurloNews>().neurloNewsProfil+" FOR:"+clone);
                    clone.GetComponent<FillNeurloNews>().neurloNewsProfil = g.GetComponent<FillNeurloNews>().neurloNewsProfil;
                }
            }

            clone.transform.SetParent(container);
            clone.transform.localScale = Vector3.one;

            neurolonewListCrt.Add(clone);
            Debug.Log(child);
        }

        groupe.Remove(groupe[groupe.Count - 1]);
        Destroy(container.GetChild(container.childCount - 1).gameObject);
        Destroy(neurolonewListCrt[neurolonewListCrt.Count-1]);
    }

    //Delete AllData
    public void DeleteContainerData()
    {
        data = null;
        groupe = null;
        //DeleteContainerContent();
        DeleteNeuro();
    }

  

    //Unlock
    public void UnlockContainerData()
    {
        DeleteContainerContent();
        LoadNeuro();

        groupe = data.Split(';').ToList<string>();
     
        string newData = null;
        foreach (string child in groupe)
        {
            //SaveSystem.SavePlayer(child.gameObject.name);

            GameObject clone = Instantiate(prefabToClone, container.position, Quaternion.identity);
            clone.name = child.Substring(child.LastIndexOf(':') + 1);
            clone.transform.parent = container;

            Debug.Log(child);
            newData = newData + child + ";";
        }
        //Debug.Log("NewData_Before:" + newData);
        //Correction of génération, delet last character. 
        newData = newData.Substring(0, newData.Length - 1);
        //Debug.Log("NewData_after:" + newData);
        //Correction of génération, delet last empty entry. 
        groupe.Remove(groupe[groupe.Count - 1]);

        //Add News
        newData = newData + "1:CloneAdd;";

        data = newData;

        SaveNeuro();

        LoadContainerData();
    }
    public void UnlockContainerData(NeurloNewsProfil profilToUnlock)
    {
        DeleteContainerContent();
        LoadNeuro();

        groupe = data.Split(';').ToList<string>();

        string newData = null;
        foreach (string child in groupe)
        {
            GameObject clone = Instantiate(prefabToClone, container.position, Quaternion.identity);
            clone.name = child.Substring(child.LastIndexOf(':') + 1);


            //foreach (GameObject g in neurolonewList)
            //{
            //    //Badge(g);
            //    if (g.GetComponent<FillNeurloNews>().neurloNewsProfil == profilToUnlock)
            //    {
            //        clone.GetComponent<FillNeurloNews>().neurloNewsProfil = g.GetComponent<FillNeurloNews>().neurloNewsProfil;
            //    }
            //}
            clone.GetComponent<FillNeurloNews>().neurloNewsProfil = profilToUnlock;

           clone.transform.SetParent(container);
           clone.transform.localScale = Vector3.one;

            Debug.Log(child);
            newData = newData + child + ";";
        }

        //Correction of génération, delet last character. 
        newData = newData.Substring(0, newData.Length - 1);
        //Correction of génération, delet last empty entry. 
        groupe.Remove(groupe[groupe.Count - 1]);

        //Add News
        newData = newData + "1:"+ profilToUnlock+";";

        data = newData;

        SaveNeuro();
    }

    /*
     * Save/Load in Json
     */

    //Fill Data
    public void SaveNeuro()
    {
        neuroData = data;
        Debug.Log("Save neuro- neuroData: " + neuroData);
        SaveNeuroData(this);
        Debug.Log("Save neuro- SND: " + this);
    }
    public void LoadNeuro()
    {
        LoadNeuroData(this);
        Debug.Log("Load neuro- neurodata: " + neuroData);
        data = neuroData;

    }
    public void DeleteNeuro()
    {
        neuroData = null;


        SaveNeuroData(this);
    }

    //Write/Read file
    private static void SaveNeuroData(SaveNeuroNewsManager _playerDataSystem)
    {
        Debug.Log("SaveNeuroData - sst: " + _playerDataSystem.neuroData);
        SaveNeuroData snd = new SaveNeuroData();
        Debug.Log("SaveNeuroData - snd: " + snd._stringNeuro);

        _playerDataSystem.PopulateNeuroSaveData(snd);

        if (FileManager.WriteToFile("neuro.dat", snd.ToJson()))
        {
            Debug.Log("Save neuro Succesfull: " + snd._stringNeuro);
        }
    }
    private static void LoadNeuroData(SaveNeuroNewsManager _playerDataSystem)
    {
        if (FileManager.LoadFromFile("neuro.dat", out var json))
        {
            SaveNeuroData snd = new SaveNeuroData();
            snd.LoadFromJson(json);

            _playerDataSystem.LoadNeuroFromSaveData(snd);
            Debug.Log("Load neuro Succesfully: " + snd._stringNeuro);
        }
    }

    //update File
    public void PopulateNeuroSaveData(SaveNeuroData _saveNeuroData)
    {
        _saveNeuroData._stringNeuro = neuroData;
    }
    public void LoadNeuroFromSaveData(SaveNeuroData _saveNeuroData)
    {
        neuroData = _saveNeuroData._stringNeuro;
    }
}