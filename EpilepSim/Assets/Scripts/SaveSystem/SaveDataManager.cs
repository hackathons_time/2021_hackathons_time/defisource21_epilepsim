﻿using System.Collections.Generic;
using UnityEngine;
using static SaveNeuroData;


public static class SaveDataManager
{
    public static void SaveJsonData(IEnumerable<ISaveable> a_Saveables)
    {
        SaveData sd = new SaveData();
        foreach (var saveable in a_Saveables)
        {
            saveable.PopulateSaveData(sd);
        }

        if (FileManager.WriteToFile("SaveData01.dat", sd.ToJson()))
        {
            Debug.Log("Save successful");
        }
    }
    
    public static void LoadJsonData(IEnumerable<ISaveable> a_Saveables)
    {
        if (FileManager.LoadFromFile("SaveData01.dat", out var json))
        {
            SaveData sd = new SaveData();
            sd.LoadFromJson(json);

            foreach (var saveable in a_Saveables)
            {
                saveable.LoadFromSaveData(sd);
            }
            
            Debug.Log("Load complete");
        }
    }

    /*
     * 
     */
    public static void SaveNeuroJsonData(IEnumerable<ISavableNeuro> a_Saveables)
    {
        SaveNeuroData snd = new SaveNeuroData();
        foreach (var saveable in a_Saveables)
        {
            saveable.PopulateNeuroSaveData(snd);
        }

        if (FileManager.WriteToFile("SaveData01.dat", snd.ToJson()))
        {
            Debug.Log("Save Neuro successful");
        }
    }

    public static void LoadNeuroJsonData(IEnumerable<ISavableNeuro> a_Saveables)
    {
        if (FileManager.LoadFromFile("SaveData01.dat", out var json))
        {
            SaveNeuroData snd = new SaveNeuroData();
            snd.LoadFromJson(json);

            foreach (var saveable in a_Saveables)
            {
                saveable.LoadNeuroFromSaveData(snd);
            }

            Debug.Log("Load Neurocomplete");
        }
    }

}
