
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    //Profil player
    public string _paramNamePlayer;
    public int _paramGender;
    public int _paramOrientation;
    public int _paramAge;
    public int _paramCriseId;
    public int _paramTraitement;
    public int _paramStress;
    public int _paramLackSleep;
    public int _paramPhotosensibility;
    public int _paramHeat;
    public int _paramIrregularMeal;
    public int _paramLackMedic;
    public int _Alcohol;
    public int _Drug;

    public string ToJson()
    {
        return JsonUtility.ToJson(this);
    }

    public void LoadFromJson(string _Json)
    {
        JsonUtility.FromJsonOverwrite(_Json, this);
    }
  
}

public interface ISaveable
{
    void PopulateSaveData(SaveData _saveData);
    void LoadFromSaveData(SaveData _saveData);
}
