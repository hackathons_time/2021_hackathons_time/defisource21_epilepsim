using UnityEngine;

[System.Serializable]
public class SaveNeuroData 
{

    public string _stringNeuro;
    public string _DesccriptionNeuro;
    

    public string ToJson()
    {
        return JsonUtility.ToJson(this);
    }

    public void LoadFromJson(string _Json)
    {
        JsonUtility.FromJsonOverwrite(_Json, this);
    }

    public interface ISavableNeuro
    {
        void PopulateNeuroSaveData(SaveNeuroData _saveNeuroData);
        void LoadNeuroFromSaveData(SaveNeuroData _saveNeuroData);
    }
}
