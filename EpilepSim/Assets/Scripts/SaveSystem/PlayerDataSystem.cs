using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using TMPro;
using System;

public class PlayerDataSystem : MonoBehaviour, ISaveable
{
    [Header("Interface to Save Data")]
    [Space(5)]
    #region Profil Data 
    [Header("Generals Interface ")]
    //interface profil
    [SerializeField] private TMP_InputField namePlayerInput = default;
    [SerializeField] private GameObject namePlayerGO = default;
    [SerializeField] private List<Toggle> genderToggles = default;
    [SerializeField] private List<Toggle> orientationToggle = default;
    [SerializeField] private TMP_InputField ageText = default;
    [Header("Epilepsie Interface ")]
    [SerializeField] private List<Toggle> CrisisToggle = default;
    [SerializeField] private TMP_Dropdown cureDropdown = default;
    [Header("Factors Interface ")]
    //Interface Factors
    [SerializeField] private Slider stressSlider = default;
    [SerializeField] private Slider lackOfSleeplider = default;
    [SerializeField] private Slider photosensibilitySlider = default;
    [SerializeField] private Slider heatSlider = default;
    [SerializeField] private Slider lackOfMealSlider = default;
    [SerializeField] private Slider lackOfMedicSlider = default;
    [SerializeField] private Slider AlcoholSlider = default;
    [SerializeField] private Slider DrugSlider = default;

    //data General Profil
    private string namePlayerData = default;
    private int genderIDData = default;
    private int orientationIDData = default;
    private int ageData = default;
    //data General Profil
    private int crisisIDData = default;
    private int cureIDData = default;
    //data Factors
    private int stresslevelData = default;
    private int lackSleepLevelData = default;
    private int photosensibilityLevelData = default;
    private int heatLevelData = default;
    private int lackMealLevelData = default;
    private int lackMedicLevelData = default;
    private int AlcoholLevelData = default;
    private int drugLevelData = default;
    #endregion

    #region Game progression Data
    #endregion


    void Start()
    {

        if (PlayerPrefs.HasKey("HaveData"))
        {
            loadProfil();
            ActiveGooDToggle(genderToggles, genderIDData);
            ActiveGooDToggle(orientationToggle, orientationIDData);
            ActiveGooDToggle(CrisisToggle, crisisIDData);
        }
        else
        {
            DeleteProfil();
            loadProfil();
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)){
            loadProfil();
            //Set Toggle
            ActiveGooDToggle(genderToggles, genderIDData);
        }
    }

    
    //Set data with toggle
    public void ToggleSetGenre(int ToggleIndex/*, Transform toggleParent*/)
    {
        genderIDData = ToggleIndex;
    }
    //Set data with toggle
    public void ToggleSetOrientation(int ToogleIndex)
    {
        orientationIDData = ToogleIndex;
    }
    //Set data with toggle
    public void ToggleSetCrisis(int ToogleIndex)
    {
        crisisIDData = ToogleIndex;
    }

    //Get On toggle From Data{
    public void ActiveGooDToggle(List<Toggle> listToggles, int DataToSet)
    {
     for(int i = 0; i<listToggles.Count; ++i)
        {
            if (i == DataToSet)
            {
                listToggles[i].isOn = true;
                Debug.Log("Concerne : " + listToggles + " active :" + listToggles[i].name);
            }
            else
            {
                listToggles[i].isOn = false;
            }
        }
    }

    /****************
     * Saving And Loading
     */

    public void SaveProfil()
    {
        //Create Key
        PlayerPrefs.SetInt("HaveData", 1);

        //interface to data

        //General
        namePlayerData = namePlayerGO.GetComponent<TMP_InputField>().text;
        //genreToggle give by Toggle
        //orientationIDData  give by Toggle
        if (int.TryParse(ageText.GetComponent<TMP_InputField>().text.ToString(), out Int32 tmpAge))
        {
            ageData = (int)tmpAge;
        }
        else
        {
            ageData = -1;
        }
        ////Epilepsie
        //crisisIDData give by Toggle
        cureIDData = cureDropdown.value;
        stresslevelData = (int)stressSlider.value;
        lackSleepLevelData = (int) lackOfSleeplider.value;
        photosensibilityLevelData = (int)photosensibilitySlider.value;
        heatLevelData = (int)heatSlider.value;
        lackMealLevelData = (int)lackOfMealSlider.value;
        lackMedicLevelData = (int)lackOfMedicSlider.value;
        AlcoholLevelData = (int)AlcoholSlider.value;
        drugLevelData = (int)DrugSlider.value;

        Debug.Log("JZ_saveProfil\n" +
            "namePlayer : " + namePlayerData + "\n"+
            "Genre : " + genderIDData + "\n" +
            "Orientation : " + orientationIDData + "\n" + 
            "age : " + ageData + "\n" +
            "Crisis : " + crisisIDData + "\n" +
            "cureID : " + cureIDData + "\n" +
            "stress : " + stresslevelData + "\n" +
            "lackSleep : " + lackSleepLevelData + "\n" +
            "Photo : " + photosensibilityLevelData + "\n" +
            "heal : " + heatLevelData + "\n" +
            "LackMeal : " + lackMealLevelData + "\n" +
            "lackMedic : " + lackMedicLevelData + "\n" +
            "Alcohol : " + AlcoholLevelData + "\n" +
            "Drug : " + drugLevelData + "\n"
            );
        //then SaveIt
        SaveJsonData(this);
    }

    public void loadProfil()
    {
        //Load data
        LoadJsonData(this);

        Debug.Log("JZ_Load Profil\n" +
       "namePlayer : " + namePlayerData + "\n" +
       "Genre : " + genderIDData + "\n" +
       "Orientation : " + orientationIDData + "\n" +
       "age : " + ageData + "\n" +
       "Crisis : " + crisisIDData + "\n" +
       "cureID : " + cureIDData + "\n" +
       "stress : " + stresslevelData + "\n" +
       "lackSleep : " + lackSleepLevelData + "\n" +
       "Photo : " + photosensibilityLevelData + "\n" +
       "heal : " + heatLevelData + "\n" +
       "LackMeal : " + lackMealLevelData + "\n" +
       "lackMedic : " + lackMedicLevelData + "\n" +
       "Alcohol : " + AlcoholLevelData + "\n" +
       "Drug : " + drugLevelData + "\n"
       );
        //Data to inteface

        //General
        namePlayerGO.GetComponent<TMP_InputField>().text = namePlayerData;
        //namePlayerInput.text = namePlayerData;
        //genreToggle
        //genderIDData = _saveData._paramGender;
        //orientationIDData = _saveData._paramOrientation;
        ageText.GetComponent<TMP_InputField>().text = ageData.ToString();
        ////Epilepsie
        //crisisIDData = _saveData._paramCriseId;
        cureDropdown.value = cureIDData;
        stressSlider.value = stresslevelData;
        lackOfSleeplider.value = lackSleepLevelData;
        photosensibilitySlider.value = photosensibilityLevelData;
        heatSlider.value = heatLevelData;
        lackOfMealSlider.value = lackMealLevelData;
        lackOfMedicSlider.value = lackMedicLevelData;
        AlcoholSlider.value = AlcoholLevelData;
        DrugSlider.value = drugLevelData;
    }

    //public static void DeleteProfil()
    //{
    //    DeleteProfil(PlayerDataSystem.this);
    //}

    //public  void DeleteProfil(PlayerDataSystem _playerDataSystem)
    //{
    //    PlayerPrefs.DeleteKey("HaveData");

    //    _playerDataSystem.namePlayerData = " ";
    //    //genreToggle give by Toggle
    //    //orientationIDData  give by Toggle
    //    _playerDataSystem.ageData = 0;
    //    ////Epilepsie
    //    //crisisIDData give by Toggle
    //    _playerDataSystem.cureIDData = 0;
    //    _playerDataSystem.stresslevelData = 5;
    //    _playerDataSystem.lackSleepLevelData = 5;
    //    _playerDataSystem.photosensibilityLevelData = 5;
    //    _playerDataSystem.heatLevelData = 5;
    //    _playerDataSystem.lackMealLevelData = 5;
    //    _playerDataSystem.lackMedicLevelData = 5;
    //    _playerDataSystem.AlcoholLevelData = 5;
    //    _playerDataSystem.drugLevelData = 5;

    //    //then SaveIt
    //    SaveJsonData(_playerDataSystem);
    //}

    public void DeleteProfil()
    {
        PlayerPrefs.DeleteKey("HaveData");

         namePlayerData = " ";
        //genreToggle give by Toggle
        //orientationIDData  give by Toggle
         ageData = 0;
        ////Epilepsie
        //crisisIDData give by Toggle
         cureIDData = 0;
         stresslevelData = 5;
         lackSleepLevelData = 5;
         photosensibilityLevelData = 5;
         heatLevelData = 5;
         lackMealLevelData = 5;
         lackMedicLevelData = 5;
         AlcoholLevelData = 5;
         drugLevelData = 5;

        //then SaveIt
        SaveJsonData(this);
    }



    //write file storage
    private static void SaveJsonData(PlayerDataSystem _playerDataSystem)
    {
       
        SaveData sd = new SaveData();

        _playerDataSystem.PopulateSaveData(sd);

        if (FileManager.WriteToFile("SaveData.dat", sd.ToJson()))
        {
            Debug.Log("Save Succesfull");
        }
    }

    //Load file storage
    private static void LoadJsonData(PlayerDataSystem _playerDataSystem)
    {
        if (FileManager.LoadFromFile("SaveData.dat", out var json))
        {
            SaveData sd = new SaveData();
            sd.LoadFromJson(json);

            _playerDataSystem.LoadFromSaveData(sd);
            Debug.Log("Load Succesfully");
        }
    }

    //Read Data from storage
    public void LoadFromSaveData(SaveData _saveData)
    {
        //General
        namePlayerData = _saveData._paramNamePlayer;
        genderIDData = _saveData._paramGender;
        orientationIDData = _saveData._paramOrientation;
        ageData =_saveData._paramAge;
        //Epilepsie
        crisisIDData = _saveData._paramCriseId;
        cureIDData = _saveData._paramTraitement;
        //Factors
        stresslevelData= _saveData._paramStress;
        lackSleepLevelData = _saveData._paramLackSleep;
        photosensibilityLevelData = _saveData._paramPhotosensibility;
        heatLevelData = _saveData._paramHeat;
        lackMealLevelData = _saveData._paramIrregularMeal;
        lackMedicLevelData = _saveData._paramLackMedic;
        AlcoholLevelData = _saveData._Alcohol;
        drugLevelData = _saveData._Drug;
    }

    //Load Data for Storage
    public void PopulateSaveData(SaveData _saveData)
    {
        //General
        _saveData._paramNamePlayer = namePlayerData;
        _saveData._paramGender = genderIDData;
        _saveData._paramOrientation = orientationIDData;
        _saveData._paramAge = ageData;
        //Epilepsie
        _saveData._paramCriseId = crisisIDData;
        _saveData._paramTraitement = cureIDData;
        //Factors
        _saveData._paramStress = stresslevelData;
        _saveData._paramLackSleep = stresslevelData;
        _saveData._paramPhotosensibility = photosensibilityLevelData;
        _saveData._paramHeat = heatLevelData;
        _saveData._paramIrregularMeal = lackMealLevelData;
        _saveData._paramLackMedic = lackMedicLevelData;
        _saveData._Alcohol = AlcoholLevelData;
        _saveData._Drug = drugLevelData;
    }
}
