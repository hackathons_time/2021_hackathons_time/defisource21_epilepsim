using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System.Linq;
using static SaveNeuroData;

public  class SaveSystemTests : MonoBehaviour, ISavableNeuro
{
    [SerializeField] private Transform container;
    [SerializeField] private GameObject prefabToClone;
    [SerializeField] private TextMeshProUGUI debugText;

    //Serializefile to Debug
    [SerializeField] private string data = null;
    [SerializeField] private List<string> groupe = null;

    //Data
    [SerializeField] private string neuroData = null;

    //Neuro
    [SerializeField] private Object[] neurolonewList = default;

    // Start is called before the first frame update
    void Start()
    {
        data = null;
        groupe = null;

        neurolonewList = Resources.LoadAll("Neurolonews", typeof(GameObject));
    }


    /*//Buttons
     */
    //Bouton Rest Scene
    public void ResetScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        debugText.text = "Reset Scene";
    }

    //Delete
    public void DeleteContainerContent()
    {
        Debug.Log("Delete");
        if (container.childCount > 0)
        {
            foreach (Transform child in container.transform)
            {
                Debug.Log("Deleting:" + child);
                Destroy(child.gameObject);
            }
            debugText.text = "Delete Content";
        }
    }

    //Add Object
    public void AddContainerContent()
    {
        
        int randomIndex = Random.Range(0, neurolonewList.Count() - 1);
        Debug.Log("randomIndex tmp: " + randomIndex);
        GameObject clone = Instantiate(prefabToClone, container.position, Quaternion.identity);
        clone.name = "Clone_" + container.childCount;
        GameObject tmpProfil = neurolonewList[randomIndex] as GameObject;
        foreach (GameObject g in neurolonewList)
        {
            //Badge(g);
            if (g.GetComponent<FillNeurloNews>().neurloNewsProfil == tmpProfil.GetComponent<FillNeurloNews>().neurloNewsProfil) {
                clone.GetComponent<FillNeurloNews>().neurloNewsProfil = g.GetComponent<FillNeurloNews>().neurloNewsProfil;
            }
        }
        clone.transform.parent = container;
        debugText.text = "Add Content, now is : " + container.childCount ;

    }

    public void SaveContainerData()
    {
        string tmp = null;
        int position = 0;
        foreach (Transform child in container)
        {
            //SaveSystem.SavePlayer(child.gameObject.name);
            tmp = tmp + position + ":" + child.GetComponent<FillNeurloNews>().neurloNewsProfil + ";";
            position++;
        }
        data = tmp;
        debugText.text = "Data Save;" + tmp;

        //
        SaveNeuro();
    }

    //Load
    public void LoadContainerData()
    {
        DeleteContainerContent();
        LoadNeuro();

        Debug.Log("Loading");
        
        groupe = data.Split(';').ToList<string>();
        debugText.text = null;

        foreach (string child in groupe)
        {
            //SaveSystem.SavePlayer(child.gameObject.name);
            debugText.text = debugText.text + "|"+ groupe;

            GameObject clone = Instantiate(prefabToClone, container.position, Quaternion.identity);
            foreach (GameObject g in neurolonewList)
            {
                //Badge(g);
                if (g.GetComponent<FillNeurloNews>().neurloNewsProfil.ToString() == child.Substring(child.LastIndexOf(':') + 1))
                {
                    clone.GetComponent<FillNeurloNews>().neurloNewsProfil = g.GetComponent<FillNeurloNews>().neurloNewsProfil;
                }
            }

            clone.transform.parent = container;

            Debug.Log(child);
        }

        groupe.Remove(groupe[groupe.Count-1]);
        Destroy(container.GetChild(container.childCount-1).gameObject);
    }

    //Delete AllData
    public void DeleteContainerData()
    {
        data = null;
        groupe = null;
        DeleteContainerContent();
        DeleteNeuro();
    }

    //Unlock
    public void UnlockContainerData()
    {
        DeleteContainerContent();
        LoadNeuro();

        groupe = data.Split(';').ToList<string>();
        debugText.text = null;
        string newData = null;
        foreach (string child in groupe)
        {
            //SaveSystem.SavePlayer(child.gameObject.name);
            debugText.text = debugText.text + "|" + groupe;

            GameObject clone = Instantiate(prefabToClone, container.position, Quaternion.identity);
            clone.name = child.Substring(child.LastIndexOf(':') + 1);
            clone.transform.parent = container;

            Debug.Log(child);
            newData = newData + child +";";
        }
        Debug.Log("NewData_Before:"+ newData);
        //Correction of génération, delet last character. 
        newData= newData.Substring(0, newData.Length - 1);
        Debug.Log("NewData_after:" + newData);
        //Correction of génération, delet last empty entry. 
        groupe.Remove(groupe[groupe.Count - 1]);

        //Add News
        newData = newData+"1:CloneAdd;";

        debugText.text = newData;

        data = newData;

        SaveNeuro();

        LoadContainerData();
    }

    /*
     * Save/Load in Json
     */
    
    //Fill Data
    public void SaveNeuro()
    {
        neuroData = data;
        Debug.Log("Save neuro- neuroData: " + neuroData);
        SaveNeuroData(this);
        Debug.Log("Save neuro- SND: " + this);
    }
    public void LoadNeuro()
    {
        LoadNeuroData(this);
        Debug.Log("Load neuro- neurodata: " + neuroData);
        data = neuroData;
        
    }
    public void DeleteNeuro()
    {
        neuroData = null;


        SaveNeuroData(this);
    }
    
    //Write/Read file
    private static void SaveNeuroData(SaveSystemTests _playerDataSystem)
    {
        Debug.Log("SaveNeuroData - sst: " + _playerDataSystem.neuroData);
        SaveNeuroData snd = new SaveNeuroData();
        Debug.Log("SaveNeuroData - snd: " + snd._stringNeuro);

        _playerDataSystem.PopulateNeuroSaveData(snd);

        if (FileManager.WriteToFile("neuro.dat", snd.ToJson()))
        {
            Debug.Log("Save neuro Succesfull: " + snd._stringNeuro);
        }
    }
    private static void LoadNeuroData(SaveSystemTests _playerDataSystem)
    {
        if (FileManager.LoadFromFile("neuro.dat", out var json))
        {
            SaveNeuroData snd = new SaveNeuroData();
            snd.LoadFromJson(json);

            _playerDataSystem.LoadNeuroFromSaveData(snd);
            Debug.Log("Load neuro Succesfully: "+ snd._stringNeuro);
        }
    }

    //update File
    public void PopulateNeuroSaveData(SaveNeuroData _saveNeuroData)
    {
        _saveNeuroData._stringNeuro = neuroData;
    }
    public void LoadNeuroFromSaveData(SaveNeuroData _saveNeuroData)
    {
        neuroData = _saveNeuroData._stringNeuro;
    }
}
