using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;



public static class SaveSystem 
{
   public static void SavePlayer(string test)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/Datas/test_"+test+".data";
        Debug.Log("PATH_" + path);
        FileStream stream = new FileStream(path, FileMode.Create);

        TestData data = new TestData(test);

        formatter.Serialize(stream, data);
        stream.Close();
        
    }

    public static  TestData LoadData()
    {
        string path = Application.persistentDataPath+ "/Datas/test.data";

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
           
            FileStream stream = new FileStream(path, FileMode.Open);

            TestData data = formatter.Deserialize(stream) as TestData;

            stream.Close();

            return data;
        }
        else{
            Debug.LogError("Save file not Found in" + path);
            return null;
        }
    }
}
