using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Localization.Settings;
using UnityEngine.UI;

public class GetLanguage : MonoBehaviour
{

    public TextMeshProUGUI debug = default;
    private int indexLang = default;
    public bool isDebugging = false;

    // Start is called before the first frame update
    void Awake()
    {
        if (isDebugging)
        {
            debug.text = "1 The system is " + Application.systemLanguage;
        }
        Debug.Log("This system is in French.");

        if (PlayerPrefs.HasKey("language"))
        {
            if (isDebugging)
            {
                debug.text = "Key for langage :" + PlayerPrefs.GetInt("language");
            }
            indexLang = PlayerPrefs.GetInt("language");
            LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[PlayerPrefs.GetInt("language")];
            Debug.Log("Key for langage :" + PlayerPrefs.GetInt("language"));
        }
        else
        {
            switch (Application.systemLanguage)
            {
                case SystemLanguage.French:
                    if (isDebugging)
                    {
                        debug.text = "3 This system is in French.";
                    }
                    Debug.Log("This system is in French.");
                    indexLang = 1;
                    PlayerPrefs.SetInt("language", 1);
                    break;
                case SystemLanguage.German:
                    if (isDebugging)
                    {
                        debug.text = "3 This system is in German.";
                    }
                    Debug.Log("This system is in German.");
                    PlayerPrefs.SetInt("language", 2);
                    indexLang = 2;
                    break;
                case SystemLanguage.English:
                    if (isDebugging)
                    {
                        debug.text = "This system is in English.";
                    }
                    Debug.Log("3 This system is in English."); ;
                    PlayerPrefs.SetInt("language", 0);
                    indexLang = 0;
                    break;
                default:
                    if (isDebugging)
                    {
                        debug.text = "Not Key for langage and not Application.system";
                    }
                    Debug.Log("Not Key for langage, so set into french");
                    indexLang = 0; 
                    break;
            }
            Debug.Log("Not Key for langage");
        }
        
            StartCoroutine(StartLang(indexLang));
    }

    IEnumerator StartLang(int index)
    {
        yield return LocalizationSettings.InitializationOperation;
        LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[index];
        Debug.Log("StartLang After yield: " + LocalizationSettings.SelectedLocale.name);
    }

    public void SetLanguage()
    {
        LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[PlayerPrefs.GetInt("language")];
        Debug.Log("Set Language: " + LocalizationSettings.SelectedLocale.name);
    }

    public void LocaleSelected(int index)
    {
        LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[index];
        PlayerPrefs.SetInt("language", index);
        if (isDebugging)
        {
            debug.text = "Change local to" + index + " key is  " + PlayerPrefs.GetInt("language");
        }
    }
}
