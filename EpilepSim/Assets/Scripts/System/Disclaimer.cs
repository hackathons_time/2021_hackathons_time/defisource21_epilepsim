using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Disclaimer : MonoBehaviour
{
    [SerializeField] private string nextScene = default;
    [SerializeField] private float delayBeforeLoad = default;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitAndLoad(delayBeforeLoad));
    }

    IEnumerator WaitAndLoad(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        SceneManager.LoadScene(nextScene);
    }
}
