using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ClockScript : MonoBehaviour
{
    [SerializeField] [Range(0.001f, 1f)] 
    private float TimeRatio = 1f;                     // Game Day ratio to Real Day
    public float GameTime = 0;                          // Accumulated 'Game Time' in seconds

    // Start is called before the first frame update
    void Start()
    {
        
    }


    public void Update()
    {
        gameObject.GetComponent<TextMeshProUGUI>().text = Hours + ":" + Minutes + ":" + Seconds;
        GameTime += Time.deltaTime / TimeRatio;         // Add to 'GameTime' at 'TimeRatio' rate
    }

    public override string ToString()
    {
        //                        DAYS,  HOUR : MINS : SECS . MS
        return string.Format("{0} Days, {1:00}:{2:00}:{3:00}.{4:000}", Days, Hours, Minutes, Seconds, Milliseconds);
    }

    public int Milliseconds
    {
        get
        {
            return (int)((GameTime - TotalSeconds) * 1000);
        }
    }

    public int Seconds
    {
        get
        {
            return TotalSeconds % 60;
        }
    }

    public int Minutes
    {
        get
        {
            return TotalSeconds / 60 % 60;
        }
    }

    public int Hours
    {
        get
        {
            return TotalSeconds / (60 * 60) % 60 % 24;
        }
    }

    public int Days
    {
        get
        {
            return TotalSeconds / (60 * 60 * 24);
        }
    }

    public int TotalSeconds
    {
        get
        {
            return (int)GameTime;
        }
    }

}