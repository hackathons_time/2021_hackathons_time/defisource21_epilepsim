using System.Collections;
using System.Collections.Generic;
using UnityEngine.Localization;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization.Settings;
using TMPro;

public class SetLangage : MonoBehaviour
{

    public List<string> options = new List<string>();
    public TextMeshProUGUI debug = default;
    public bool isDebugging = false;

    public void Start()
    {
        if (isDebugging)
        {
            debug.text = "1 The system is " + Application.systemLanguage;
        }
       
        StartCoroutine(StartLang());
    }

    IEnumerator StartLang()
    {
        //Debug.Log("StartLang before yield");
        // Wait for the localization system to initialize, loading Locales, preloading etc.
        yield return LocalizationSettings.InitializationOperation;
        //Debug.Log("StartLang After yield");
        //// Generate list of available Locales
        //var options = new List<string>();

        //for (int i = 0; i < LocalizationSettings.AvailableLocales.Locales.Count; ++i)
        //{
        //    var locale = LocalizationSettings.AvailableLocales.Locales[i];
        //    Debug.Log("JZ_Table : "+ locale.name +" At index : "+ i);
        //    options.Add(locale.name);
        //}
        //Debug.Log("StartLang End");
        if (isDebugging)
        {
            debug.text = "2 The system is " + Application.systemLanguage;
        }
        CheckKey();
    }

     public void LocaleSelected(int index)
    {
        LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[index];
        PlayerPrefs.SetInt("language", index);
    }

  

    public void CheckKey()
    {
        if (PlayerPrefs.HasKey("language"))
        {
            LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[PlayerPrefs.GetInt("language")];
        }
        else
        {
            switch (Application.systemLanguage)
            {
                case SystemLanguage.French:
                    if (isDebugging)
                    {
                        debug.text = "3 This system is in French.";
                    }
                   Debug.Log("This system is in French.");
                    PlayerPrefs.SetInt("language", 1);
                    break;
                case SystemLanguage.German:
                    if (isDebugging)
                    {
                        debug.text = "3 This system is in German.";
                    }
                        Debug.Log("This system is in German.");
                    PlayerPrefs.SetInt("language", 2);
                    break;
                case SystemLanguage.English:
                    if (isDebugging)
                    {
                        debug.text = "This system is in English.";
                    }
                        Debug.Log("3 This system is in English."); ;
                    PlayerPrefs.SetInt("language", 0);
                    break;
                default:
                    if (isDebugging)
                    {
                        debug.text = "Not Key for langage and not Application.system";
                    }
                        Debug.Log("Not Key for langage");
                    break;
            }
            LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[PlayerPrefs.GetInt("language")];
        }
    }
}