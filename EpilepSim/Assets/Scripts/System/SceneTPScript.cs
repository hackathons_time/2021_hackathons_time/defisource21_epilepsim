using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTPScript : MonoBehaviour
{
    //public string tpName;
    //public static string previousScene { get; set; }

    [SerializeField] private Vector3 appearPosition;
    [SerializeField] private Object sceneToTP;
   // [SerializeField] private string leveName; 
    private StatusBarsManager statusBarsManager = default;
    //Use this to use String System to load
    [SerializeField] private bool UseString = false, uselength = false;
    public enum SceneList { IN_Game, IN_SecondSceneTest, OUT_ThirdSceneTest };
    [Tooltip("S�lection vers quelle niveau tu veux te tp")]
    public SceneList goTo;
    [SerializeField] private enum ArrivedDirection
    {
        TOP,
        DOWN,
        LEFT,
        RIGHT
    };
    [Tooltip("La direction depuis laquelle tu viens (Exemple: quand tu sors de la chambre, tu viens depuis la droite)")]
    [SerializeField] private ArrivedDirection fromWhereDirection;

    [Header("Effect sound")]
    private AudioMenu audioManager = default;



    private void Awake()
    {
        gameObject.name = "TP_" + goTo.ToString();
    }
    // Start is called before the first frame update
    void Start()
    {
        //get ref
        audioManager = GameObject.FindObjectOfType<AudioMenu>();

        statusBarsManager = GameObject.FindObjectOfType<StatusBarsManager>().GetComponent<StatusBarsManager>();
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        audioManager = GameObject.FindObjectOfType<AudioMenu>();

        if (collision.gameObject.tag == "Player")
        {
            //PlayerPrefs.DeleteKey("previousScene");

            PlayerPrefs.SetString("PreviousScene", SceneManager.GetActiveScene().name);
          
            statusBarsManager.SaveSatusbar();

            //Effect Sound
            //audioManager.UseDoor(); //Use hardcore methode
            audioManager.PlayEffectSound(audioManager.GetAudioClipFromDict("door")); // Use DynamicMethod
            if (!uselength)
            {
                StartCoroutine(WaitOnMusicToTP(0f));
            }
            else
            {
                StartCoroutine(WaitOnMusicToTP(audioManager.GetAudioClipFromDict("door").length));
            }

            

            //if (UseString)
            //{
            //    SceneManager.LoadScene(goTo.ToString());
            //}
            //else
            //{
            //    SceneManager.LoadScene(sceneToTP.name);
            //}
        }
    }

    public Vector3 setAppearPosition()
    {
        switch (fromWhereDirection)
        {
            case ArrivedDirection.TOP:
                appearPosition = transform.position + new Vector3(0, 1f, 0);
                break;
            case ArrivedDirection.DOWN:
                appearPosition = transform.position - new Vector3(0, 1.3f, 0);
                break;
            case ArrivedDirection.LEFT:
                appearPosition = transform.position - new Vector3(1.5f, 0, 0);
                break;
            case ArrivedDirection.RIGHT:
                appearPosition = transform.position + new Vector3(1.5f, 0, 0);
                break;
        }

        return appearPosition;
    }


    IEnumerator WaitOnMusicToTP(float secondes)
    {
        //Stop Player
        GameObject.Find("Joystick").gameObject.SetActive(false);
        yield return new WaitForSeconds(secondes/2);
        SceneManager.LoadScene(goTo.ToString());
    }
}

