using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MenuNavigation : MonoBehaviour
{
    [SerializeField] public List<Transform> menuPannels = default;
    [SerializeField] private bool haveData = false;
    [SerializeField] private GameObject btnResetData = default;
    [SerializeField] private string gameLevelName = default;
    [SerializeField] private string profilLevelName = default;
    [SerializeField] private bool isUsingResetdata = false;
    private PlayerDataSystem playerDataSystem = default;

    [Header("Audio")]
    private AudioMenu audioManager = default;
    [SerializeField] private Slider ambientSlider = default;
    [SerializeField] private Slider effectSlider = default;

    //data
    private SaveNeuroNewsManager saveNeuroNewsManager =  new SaveNeuroNewsManager();

    // Start is called before the first frame update
    void Start()
    {
        //get ref
        audioManager = GameObject.FindObjectOfType<AudioMenu>();

        
        if(SceneManager.GetActiveScene().name == "Menu")
        {
            //Ajudt Audio Slider
            SetAudioValue();
        }
      

        // Disable screen dimming
        // A DEPLACER DANS LE GAME MANAGER
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        ShowPannel(0);

        if (PlayerPrefs.HasKey("HaveData"))
        {
            if (PlayerPrefs.GetInt("haveData") == 1)
            {
                haveData = true;
                //Debug.Log("JZ_GetPlayerprefabsGetInt 1 ? :+");
            }
            else
            {
                haveData = true;
            }
        }
        else
        {
            haveData = false;
        }

        playerDataSystem = new PlayerDataSystem();

        //Active/Deactive
        if (isUsingResetdata)
        {
            btnResetData.GetComponent<Button>().interactable = haveData;
        }



    }

    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log("JZ P Pressed");
            PlayerPrefs.DeleteKey("HaveData");
        }

        if (PlayerPrefs.HasKey("HaveData"))
        {
            if (PlayerPrefs.GetInt("haveData") == 1)
            {
                Debug.Log("KEY_Have key ");
            } 
            else
            {
                Debug.Log("KEY_Have but not 1 now is : " + PlayerPrefs.GetInt("haveData"));
            }
        }
        else
        {
            Debug.Log("KEY_not found");
        }
    }

    public void ShowPannel(int menuPannelsIndex)
    {
        foreach(Transform child in menuPannels)
        {
            child.gameObject.SetActive(false);
        }

        menuPannels[menuPannelsIndex].gameObject.SetActive(true);
    }

    /// <summary>
    /// Generic methode to change scene
    /// </summary>
    /// <param name="nextScene"></param>
    public void LoadScene(string nextScene)
    {
        SceneManager.LoadScene(nextScene);
    }

    /// <summary>
    /// Method for Play Button from Menu choice between LastScene or GameLevel
    /// </summary>
    public void LoadGame()
    {
        //Debug.Log("JZ LoadGame Pressed");
        if (PlayerPrefs.HasKey("HaveData"))
        {
            Debug.Log("JZ LoadGame Have Data");
            if (PlayerPrefs.HasKey("LastSceneName"))
            {
                //Debug.Log("JZ LoadGame use lasteSceneName :"+ PlayerPrefs.GetString("LastSceneName"));
                FromMenuToGame();
                SceneManager.LoadScene(PlayerPrefs.GetString("LastSceneName"));
            }
            else
            {
               //Debug.Log("JZ LoadGame Dont LastSceneName");
                LoadScene(gameLevelName);
            }
        }
        else
        {
            //Debug.Log("JZ LoadGame Dont have Data");
            LoadScene(profilLevelName);
        }
    }

    /// <summary>
    /// Method to lauch game from Epilectique Profil
    /// </summary>
    public void LoadGameFromProfil()
    {
        FromMenuToGame();
      
        //Debug.Log("JZ LoadGame Pressed");
        if (PlayerPrefs.HasKey("HaveData"))
        {
            Debug.Log("JZ LoadGame Have Data");
            if (PlayerPrefs.HasKey("LastSceneName"))
            {
                //Debug.Log("JZ LoadGame use lasteSceneName :"+ PlayerPrefs.GetString("LastSceneName"));
                SceneManager.LoadScene(PlayerPrefs.GetString("LastSceneName"));
            }
            else
            {
                Debug.LogError("JZ LoadGame Dont LastSceneName"); 
                LoadScene(gameLevelName);
            }
        }
        else
        {
            //Debug.Log("JZ LoadGame Dont LastSceneName");
            LoadScene(gameLevelName);
        }

    }

    public void SaveEpileptiqueProfil()
    {
        PlayerPrefs.SetInt("HaveData", 1);
    }

    public void SaveMenuParameter()
    {
        //PlayserPref fore Sounds 
        ShowPannel(0);
    }

    public void SetAmbientVolume(float newVolume)
    {
        audioManager.SetAmbientVolume(newVolume);
    }
    public void SetEffectVolume(float newVolume)
    {
        audioManager.SetEffecttVolume(newVolume);
    }

    /// <summary>
    /// Ajduste the slider with the saved value
    /// </summary>
    public void SetAudioValue()
    {
        ambientSlider.value = audioManager.GetAmbientVolume();
        effectSlider.value = audioManager.GetEffecttVolume();
    }


    public void DeleteData()
    {
        PlayerPrefs.DeleteKey("HaveData");
        PlayerPrefs.DeleteKey("language");
        PlayerPrefs.DeleteKey("LastSceneName");
        PlayerPrefs.DeleteKey("LastPlayerPosition");
        PlayerPrefs.DeleteKey("Session");
        PlayerPrefs.DeleteKey("Energy");
        PlayerPrefs.DeleteKey("Fun");
        PlayerPrefs.DeleteKey("Money");
        PlayerPrefs.DeleteAll();
        playerDataSystem.DeleteProfil();
        btnResetData.GetComponent<Button>().interactable = false;
        saveNeuroNewsManager.DeleteContainerData();
    }

    public void Quit()
    {
        Application.Quit();
    }


    public void setParametersPanel(Transform paramPanel)
    {
        menuPannels[1] = paramPanel;
    }


    /// <summary>
    /// Set Session variable into null to know when you come into game from Menu.
    /// </summary>
    public void FromMenuToGame()
    {
        PlayerPrefs.DeleteKey("Session");
        PlayerPrefs.DeleteKey("QuestNumber)"); //Reset Quest when quit Session
        PlayerPrefs.DeleteKey("crtNeurolonewsPopUp");//Reset Neuro when quit Session
    }

}
