﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[Serializable]
public struct ClipDictionnary
{
    public int id;
    public string nameKey;
    public AudioClip audioClip;
}

public class AudioMenu : MonoBehaviour
{
    private AudioSource audioSource;
    private AudioSource effectSource;
    [Header("Ambient Sounds")]
    [SerializeField] private List<AudioClip> ambientClips;
    [Header("effect Sounds")]
    [SerializeField] private List<AudioClip> effectClips;
    [Header("effect Sounds Dictionnary")]
    public List<ClipDictionnary> list = new List<ClipDictionnary>(); //Dans l'idée utilisé un dictionnaire pour appelé la clef et changer le son.
  

    private bool canPlayAmbientMusic { get; set; }

    void Awake()
    {

        /*
         *Check if their are more thna one instance of this gameOjbect, it will be destroy. Otherwise use DontDestroyOnload
         */
        if (GameObject.FindObjectsOfType<AudioMenu>().Length > 1)
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
        }
       


        /*When you have many same component you can select one using order in inspector 0->first, 1-Second etc
         *
         **/
        audioSource = GetComponents<AudioSource>()[0];
        effectSource = GetComponents<AudioSource>()[1];

        canPlayAmbientMusic = true;

        //Check Volume
        CheckVolume();

        if (!audioSource.isPlaying)
        {
            audioSource.clip = ambientClips[0];
            audioSource.Play();
        }
    }

    //Check when ambient Sound is Stop. 
    public void FixedUpdate()
    {
        if (!audioSource.isPlaying && canPlayAmbientMusic)
        {
            PlayMusicGame();
        }
    }
    public void PlayMusic()
    {
        if (audioSource.isPlaying) return;
        audioSource.Play();
    }

    public void PlayMusicGame()
    {
        audioSource.clip = ambientClips[UnityEngine.Random.Range(0,ambientClips.Count)];
        if (audioSource.isPlaying) return;
        audioSource.Play();
    }

    
    public void StopMusic()
    {
        audioSource.Stop();
    }

    public void StopEffect()
    {
        effectSource.Stop();
    }

    public void PlayEndSound()
    {
        effectSource.volume = 0;
        StartCoroutine(FadeAudio(audioSource, 2, 0));
        effectSource.clip = ambientClips[2];
        effectSource.Play();
        StartCoroutine(FadeAudio(effectSource, 1, 1));
    }

    private IEnumerator FadeAudio(AudioSource audioSource, float duration, float targetVolume)
    {

        float currentTime = 0;
        float start = audioSource.volume;
        Debug.Log("Fade Audio: " + currentTime + "  ||  " + audioSource.volume);


        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
            yield return null;
        }
        // Stop music if changing scene //

        if (targetVolume == 0 && audioSource.volume == 0)
        {
            audioSource.Stop();
        }

        yield break;
    }

    /*
    * Set Volume
    */
    public void SetAmbientVolume(float newVolume)
    {
        audioSource.volume = newVolume;
        PlayerPrefs.SetFloat("AmbientVolume", audioSource.volume);
    }
    public float GetAmbientVolume()
    {
        return audioSource.volume;
    }

    public void SetEffecttVolume(float newVolume)
    {
        effectSource.volume = newVolume;
        PlayerPrefs.SetFloat("EffectVolume", effectSource.volume);
    }
    public float GetEffecttVolume()
    {
        return effectSource.volume;
    }

    public float GetlenghtEffecSoundsNbr(int index)
    {
        if(index < 0)
        {
            Debug.LogError("Cant Give length oe EffectSound index is under 0");
        }
        if (index > effectClips.Count)
        {
            Debug.LogError("Cant Give length oe EffectSound index is out of range");
        }

        return effectClips[index].length;
    }

    public void CheckVolume()
    {
        if (PlayerPrefs.HasKey("AmbientVolume"))
        {
            audioSource.volume = PlayerPrefs.GetFloat("AmbientVolume");
        }
        else
        {
            SetAmbientVolume(0.5f);
        }

        if (PlayerPrefs.HasKey("EffectVolume"))
        {
            effectSource.volume = PlayerPrefs.GetFloat("EffectVolume");
        }
        else
        {
            SetEffecttVolume(1f);
        }
    }

    /*
    * Effect Sounds
    */
    public void PressDialog()
    {
        effectSource.clip = effectClips[1];
        effectSource.Play();
    }

    public void UseDoor()
    {
        effectSource.clip = effectClips[0];
        effectSource.Play();
    }

    public void PlayEffectSound(AudioClip newEffect)
    {
        effectSource.clip = newEffect;
        effectSource.Play();
    }

    public AudioClip GetAudioClipFromDict(string name)
    {
        AudioClip clip = null;
        
        foreach(ClipDictionnary child in list)
        {
            if(child.nameKey == name)
            {
                clip = child.audioClip;
            }
        }

        return clip;
    }
}
