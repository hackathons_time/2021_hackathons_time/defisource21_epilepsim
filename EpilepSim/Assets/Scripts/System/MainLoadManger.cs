using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainLoadManger : MonoBehaviour
{
    [Header("INPUTS")]
    [Space(10)]
    [SerializeField] private List<GameObject> inputsElements;
    [SerializeField] private GameObject inputsParents;


    [Header("USER INTERFACE")]
    [Space(10)]
    [SerializeField] private List<GameObject> uiElements;
    [SerializeField] private GameObject uiParent;
    [SerializeField] private GameObject uiCanvas;

    [Header("DIALOG")]
    [Space(10)]
    [SerializeField] private List<GameObject> dialogElements;
    [SerializeField] private GameObject dialogParent;
    [SerializeField] private GameObject dialogActivitiesParent;


    [Header("PLAYER")]
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject playerParent;
    [SerializeField] private GameObject tpPointsParent;

    [Header("ACTIVITIES MANAGER")]
    [SerializeField] private GameObject activitiesManager;

    [Header("PANELS TO LOAD")]
    [SerializeField] private int indexMenu;
    [SerializeField] private int indexParam;
    [SerializeField] private string tipsSceneName;
    [SerializeField] private string profilSceneName;
    [SerializeField] private string parameterSceneName;
    [SerializeField] private string menuSceneName;

    [SerializeField] private string tpPointName;
    [SerializeField] private bool currentSceneIsOutdoor;

    private Button pauseButton;
    private Button resumeButton;
    private Button tipsButton;
    private Button paramButton;
    private Button profilButton;
    private Button menuButton;
    private GameObject tpPoint;


    private void Awake()
    {
        //-------------------------------------------
        // Inputs load
        //-------------------------------------------
        setObjectsInParent(inputsElements, inputsParents);

        //-------------------------------------------
        // Player load
        //-------------------------------------------
        setObjectInParent(player, playerParent);

        //-------------------------------------------
        // Dialog elements load
        //-------------------------------------------
        setObjectsInParent(dialogElements, dialogParent);
        addElementsToActivities();

        //-------------------------------------------
        // UI elements load
        //-------------------------------------------
        setObjectsInParent(uiElements, uiParent);

        //-------------------------------------------
        // Set elements
        //-------------------------------------------
        setParamPanel();
        setPlayerStateElements();
        setPlayerToStatusBar();
        setHelpButtonToStatusBar();

        //-------------------------------------------
        // Add Listeners to buttons
        //-------------------------------------------
        //JZ Changes param Resolves
        uiParent.transform.GetChild(uiParent.transform.childCount-1).gameObject.SetActive(true);
        //foreach(Transform child in uiParent.transform)
        //{
        //    Debug.Log("JZ_Awake child : " + child.gameObject.name);
        //}

        //Debug.Log("JZ_Awake  childCount-1 is go name : " + uiParent.transform.GetChild(uiParent.transform.childCount - 1).gameObject.name);

        pauseButton = inputsParents.transform.GetChild(1).GetComponent<Button>();
        pauseButton.onClick.AddListener(CallParametersPanel);


        //JZ Change acces thoses buttons storing parent where have this button in child: 
        var parentParemetersBtn = uiParent.transform.Find("Parameters").GetChild(1);

        resumeButton = parentParemetersBtn.Find("Btn_Resume").GetComponent<Button>();
        resumeButton.onClick.AddListener(CallScenePanel);

        tipsButton = parentParemetersBtn.Find("Btn_Tuto").GetComponent<Button>();
        tipsButton.onClick.AddListener(LoadTipsScene);

        paramButton = parentParemetersBtn.Find("Btn_Param").GetComponent<Button>();
        paramButton.onClick.AddListener(LoadParamScene);

        profilButton = parentParemetersBtn.Find("Btn_ProfilEpi").GetComponent<Button>();
        profilButton.onClick.AddListener(LoadProfilScene);

        menuButton = parentParemetersBtn.Find("Btn_Menu").GetComponent<Button>();
        menuButton.onClick.AddListener(LoadMenuScene);

        ////JZ Changes param Resolves-> je sais pas � quoi ca sert !! -> Ok ca sert en �tat � d�sactivez la diaologBox.... pk?
        //uiParent.transform.GetChild(1).gameObject.SetActive(false);

        //addListenersToButtons();


        //In-OutDoorDetectSystem
        currentSceneIsOutdoor = SceneManager.GetActiveScene().name.StartsWith("OUT");

    }

    // Start is called before the first frame update
    //Comment By JZ
    void Start()
    {
        //IF Session exist so player use standard TP
        if (PlayerPrefs.HasKey("Session"))
        {
            placePlayer();
        }
        //paramButton.onClick.AddListener(CallParametersPanel);
        //paramButton.onClick.AddListener(() => uiCanvas.GetComponent<MenuNavigation>().ShowPannel(1));
        //paramButton.onClick.AddListener(new UnityAction(() => uiCanvas.GetComponent<MenuNavigation>().ShowPannel(1)));
        //paramButton.onClick.AddListener(delegate { Debug.Log("this is a test"); });
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void setObjectsInParent(List<GameObject> childGos, GameObject parent)
    {
        foreach(GameObject childGo in childGos)
        {
            setObjectInParent(childGo, parent);
        }
    }

    private void setObjectInParent(GameObject child, GameObject parent)
    {
        GameObject newChild = Instantiate(child);
        newChild.name = child.name;

        newChild.transform.SetParent(parent.transform, false);

        parent.SetActive(true);
        newChild.SetActive(true);
    }

    private void addElementsToActivities()
    {
        foreach (Transform activity in dialogActivitiesParent.transform)
        {
            // Set player & button
            Debug.Log("JZ_Error:" + activity.name, gameObject);
            activity.GetChild(0).GetComponent<DialogSystem>().setPlayer(playerParent.transform.GetChild(1).gameObject);
            activity.GetChild(0).GetComponent<DialogSystem>().setActionButton(inputsParents.transform.GetChild(0).gameObject);

            // Set boxes
            activity.GetChild(0).GetComponent<DialogSystem>().setDialogBox(dialogParent.transform.GetChild(0).gameObject);
            activity.GetChild(0).GetComponent<DialogSystem>().setQuestionBox(dialogParent.transform.GetChild(1).gameObject);

            // Set textes
            activity.GetChild(0).GetComponent<DialogSystem>().setPersonTextBox(dialogParent.transform.GetChild(0).gameObject.transform.Find("NameCanvas").GetComponentInChildren<Text>());
            activity.GetChild(0).GetComponent<DialogSystem>().setDialogTextBox(dialogParent.transform.GetChild(0).gameObject.transform.Find("TextCanvas").GetComponentInChildren<Text>());
            activity.GetChild(0).GetComponent<DialogSystem>().setQuestionTextBox(dialogParent.transform.GetChild(1).gameObject.transform.Find("QuestionText").GetComponentInChildren<Text>());
        }
    }

    //JZ Changes param Resolves -> use "uiParent.transform.childCount - 1" as index and this lign set parampanel in "menunavigation" and take manually using "1" at ligne 228
    private void setParamPanel()
    {
        uiCanvas.GetComponent<MenuNavigation>().setParametersPanel(uiParent.transform.GetChild(uiParent.transform.childCount - 1).transform);
    }
    
    private void setPlayerToStatusBar()
    {
        uiParent.transform.GetChild(uiParent.transform.childCount-2).GetComponent<StatusBarsManager>().SetPlayer(playerParent.transform.GetChild(1).gameObject);
    }

    private void setHelpButtonToStatusBar()
    {
        uiParent.transform.GetChild(uiParent.transform.childCount - 2).GetComponent<StatusBarsManager>().SetHelpButton(inputsParents.transform.GetChild(2).gameObject);
    }

    private void setPlayerStateElements()
    {
        playerParent.transform.GetChild(1).GetComponent<PlayerState>().setUiElements(uiParent.transform.GetChild(0).gameObject);
        playerParent.transform.GetChild(1).GetComponent<PlayerState>().setParamElements(uiParent.transform.GetChild(3).gameObject);
        playerParent.transform.GetChild(1).GetComponent<PlayerState>().setEventSystem(activitiesManager);
    }


    
    private void addListenersToButtons()
    {
        Debug.Log("Button 1 " + uiParent.transform.GetChild(3).GetChild(1).name);
        Debug.Log("Button 2 " + uiParent.transform.GetChild(3).GetChild(2).name);
        Debug.Log("Button 3 " + uiParent.transform.GetChild(3).GetChild(3).name);
        Debug.Log("Button 4 " + uiParent.transform.GetChild(3).GetChild(4).name);
        Debug.Log("Button 5 " + uiParent.transform.GetChild(3).GetChild(5).name);



        resumeButton.onClick.AddListener(()=>Debug.Log("Debug"));

    }


    // Calling scenes/panels methods
    public void CallScenePanel()
    {
        Debug.Log("JZ_Debug ScenePanel gets pannels : index to show :" + indexMenu);
        foreach (Transform child in uiCanvas.GetComponent<MenuNavigation>().menuPannels)
        {
            child.gameObject.SetActive(false);
            Debug.Log("JZ_Content of menupanels:y" + indexMenu);
        }
        uiCanvas.GetComponent<MenuNavigation>().ShowPannel(indexMenu);
    }
    public void CallParametersPanel()
    {
        Debug.Log("Debug CallParametersPanel");
        //JZ Changes param Resolves -> 0 is menu, 1 is parameter panel is set here in ligne 181...
        uiCanvas.GetComponent<MenuNavigation>().ShowPannel(1);
    }

    public void LoadTipsScene()
    {
        Debug.Log("Debug CallParametersPanel");
        SaveCurrentSceneNameAndPlayerPosition();
        SaveStatusBar();
        uiCanvas.GetComponent<MenuNavigation>().LoadScene(tipsSceneName);
    }

    public void LoadProfilScene()
    {
        Debug.Log("Debug CallParametersPanel");
        SaveCurrentSceneNameAndPlayerPosition();
        SaveStatusBar();
        uiCanvas.GetComponent<MenuNavigation>().LoadScene(parameterSceneName);
    }

    public void LoadParamScene()
    {
        Debug.Log("Debug CallParametersPanel");
        SaveCurrentSceneNameAndPlayerPosition();
        SaveStatusBar();
        uiCanvas.GetComponent<MenuNavigation>().LoadScene(profilSceneName);
    }

    public void LoadMenuScene()
    {
        Debug.Log("Debug CallParametersPanel");
        SaveCurrentSceneNameAndPlayerPosition();
        SaveStatusBar();
        uiCanvas.GetComponent<MenuNavigation>().LoadScene(menuSceneName);
    }

    //Use this to save position of player in Scene and the current scene 
    public void SaveCurrentSceneNameAndPlayerPosition()
    {
        player.GetComponent<SaveLastPosition>().SaveLastSceneAndPosition();
        PlayerPrefs.DeleteKey("Session"); //Kill Session
    }

    public void placePlayer()
    {
        string previousSceneName = null;
        
        //string previousSceneName = SceneTPScript.previousScene;
        if (PlayerPrefs.HasKey("PreviousScene")){
            previousSceneName = PlayerPrefs.GetString("previousScene");
        }
        else
        {
            Debug.LogError("ML_Pas de PreviousScene KEY");
        }

        if (previousSceneName!= null)
        {
            //tpPointName = SceneManager.GetActiveScene().name + "-" + PlayerPrefs.GetString("previousScene");
            tpPointName = PlayerPrefs.GetString("PreviousScene");
            Debug.Log("ML_tpPoint separate: " + SceneManager.GetActiveScene().name + "-" + PlayerPrefs.GetString("previousScene"));
            Debug.Log("ML_tpPoint name: " + tpPointName);

            try
            {
                tpPoint = tpPointsParent.transform.Find(tpPointName).gameObject;
            }
            catch(Exception e)
            {
                Debug.Log("ML_tpPoint with name " + tpPoint + " doesn't exist");
            }
          
            //Vector3 tpPointAppear = tpPoint.GetComponent<SceneTPScript>().setAppearPosition();
            Vector3 correcTpPoint = GameObject.Find("TP_" + PlayerPrefs.GetString("PreviousScene")).GetComponent<SceneTPScript>().setAppearPosition();
            Vector3 appearPosition = new Vector3(correcTpPoint.x, correcTpPoint.y, playerParent.transform.GetChild(1).transform.position.z);
            Debug.Log("ML_Appear position: " + appearPosition.x + ";" + appearPosition.y + ";" + appearPosition.z);

            playerParent.transform.GetChild(1).transform.position = appearPosition;

            //Kill Keys
            PlayerPrefs.DeleteKey("PreviousScene");
        }

        else
        {
            Debug.LogError("ML_previousSceneName is Null");
        }

    }

    public bool IsCurrentSceneOutDoor()
    {
        return currentSceneIsOutdoor;
    }

    //Call SaveStatusbar from menuNavigation to save Element. 
    public void SaveStatusBar()
    {
        uiElements[0].GetComponent<StatusBarsManager>().SaveSatusbar();
    }

}
