using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FillQuest : MonoBehaviour
{
    public TextMeshProUGUI title = default;
    public TextMeshProUGUI description = default;
    public TextMeshProUGUI award = default;

    [SerializeField] public QuestProfil quest = default;

    // Start is called before the first frame update
    void Start()
    {
        //Get Reference
        //Title
        title = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        description = transform.GetChild(1).GetComponent<TextMeshProUGUI>();
        award = transform.GetChild(2).GetComponent<TextMeshProUGUI>();

        //Fill visual  data
        title.text = quest.title;
        description.text = quest.description;
        award.text = "R�compence : " + quest.award + " Pi�ces";

        //Rename GO
        gameObject.name = "Quest : " + quest.id;
    }

    
}
