using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectAllNeurolonews : MonoBehaviour
{
    [SerializeField] private int nbrNeuroInGame = 5;
    [SerializeField] private int nbrNeuroCrtTaken = 0;
    //QuestValidation
    [Header("Quest Elements")]
    [SerializeField] private int QuestIndexNumber;
    [SerializeField] public bool canStartQuest = false;
    public int currentQuest;
    public CustomQuestManager.QuestValidation currentValidation;
    [SerializeField] private CustomQuestManager customQuestManager;
    public bool validationProgress = false;


    // Awake is called before Start
    void Awake()
    {
        customQuestManager = GameObject.FindObjectOfType<CustomQuestManager>().GetComponent<CustomQuestManager>();
    }

    private void Start()
    {
        if (PlayerPrefs.HasKey("NeurolonewsCount"))
        {
            Debug.Log("NeurolonewsCount actual COunt :" + PlayerPrefs.GetInt("NeurolonewsCount" + "/" + nbrNeuroInGame));

            nbrNeuroCrtTaken = PlayerPrefs.GetInt("NeurolonewsCount");
        }
        else
        {

            Debug.LogError("Not NeurolonewsCount");
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            nbrNeuroCrtTaken += 1;
            PlayerPrefs.SetInt("NeurolonewsCount", nbrNeuroCrtTaken);
            Debug.Log("N Pressed add Neurolonews, actual: "+nbrNeuroCrtTaken);
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            nbrNeuroCrtTaken =0;
            PlayerPrefs.DeleteKey("NeurolonewsCount");
            Debug.Log("M Pressed reset, actual: " + nbrNeuroCrtTaken);
        }
    }

    public void CheckAllNewsQuestValidation()
    {

        if (PlayerPrefs.HasKey("NeurolonewsCount"))
        {
            Debug.Log("NeurolonewsCount actual COunt :" + PlayerPrefs.GetInt("NeurolonewsCount" + "/" + nbrNeuroInGame));
            nbrNeuroCrtTaken = PlayerPrefs.GetInt("NeurolonewsCount");
            if (PlayerPrefs.GetInt("NeurolonewsCount") == nbrNeuroInGame)
                {
                Debug.Log("NeurolonewsCount compte est bon :"+ customQuestManager.GetCurrentQuestNumber() + "/"+ currentQuest);
                if (customQuestManager.GetCurrentQuestNumber() == currentQuest)
                    {
                        Debug.Log("NeurolonewsCount go validatoin");
                        ValideCurrentQuest();
                    }
                }
            }
            else
            {
               
                Debug.LogError("Not NeurolonewsCount");
            }

    }

    public void ValideCurrentQuest()
    {
        Debug.Log("NeurolonewsCount validation Process : ");
        if (customQuestManager.GetCurrentQuestNumber() == currentQuest)
        {
            Debug.Log("Quest validate");
            //GameObject.FindObjectOfType<CustomQuestManager>().GetComponent<CustomQuestManager>().CloseQuestPopUp();
            customQuestManager.ValideteQuest(currentValidation);
        }
        else
        {
            Debug.LogError("Quest Refused : " + canStartQuest + " // " + customQuestManager.GetCurrentQuestNumber() + " == " + currentQuest);
        }
    }
}
