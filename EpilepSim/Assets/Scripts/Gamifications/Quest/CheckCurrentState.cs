using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CheckCurrentState : MonoBehaviour
{
   
    [SerializeField] private GameObject newsToHIde;
    //QuestValidation
    [Header("Quest Elements")]
    [SerializeField] private int QuestIndexNumber;
    [SerializeField] public bool canStartQuest = false;
    public int currentQuest;
    public CustomQuestManager.QuestValidation currentValidation;
    [SerializeField] private CustomQuestManager customQuestManager;
    public bool validationProgress = false;

    // Awake is called before Start
    void Awake()
    {
        customQuestManager = GameObject.FindObjectOfType<CustomQuestManager>().GetComponent<CustomQuestManager>();
    }

    private void Start()
    {
        newsToHIde.SetActive(false);
    }

    public void ShowNews()
    {
        newsToHIde.SetActive(true);
        newsToHIde.GetComponent<TriggerNeuroPopUP>().ResetPlayerPrefs();
        canStartQuest = true;
    }

    public void ValideCurrentQuest()
    {
        if (canStartQuest && customQuestManager.GetCurrentQuestNumber() == currentQuest)
        {
            customQuestManager.ValideteQuest(currentValidation);
        }
        
    }

}
