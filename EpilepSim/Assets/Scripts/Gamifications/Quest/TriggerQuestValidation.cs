using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerQuestValidation : MonoBehaviour
{
    public CustomQuestManager.QuestValidation currentValidation;
    [SerializeField]private CustomQuestManager customQuestManager;

    private void Start()
    {
       customQuestManager = GameObject.FindObjectOfType<CustomQuestManager>().GetComponent<CustomQuestManager>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Touche by : " + collision.name);
        Debug.Log("JZ QUEST SYSTEM : " + currentValidation);
        if(collision.tag == "Player")
        {
            customQuestManager.ValideteQuest(currentValidation);
            Destroy(this);
        }    
    }

   
}
