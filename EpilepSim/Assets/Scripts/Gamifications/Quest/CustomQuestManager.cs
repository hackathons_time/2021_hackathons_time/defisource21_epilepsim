using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class CustomQuestManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> Quests = new List<GameObject>();
    [SerializeField] private GameObject questButton;
    [SerializeField] private GameObject queslistParent;
    [SerializeField] private Transform targetPosition, originalPosition;
    [SerializeField] private Vector3 position = new Vector3(-225, 0, 0);
    [SerializeField] private GameObject pannelToMove;
    [SerializeField] private bool canMove = false, needHide = false, popUpShow = false;
    [SerializeField] private bool questChecking = false;
    [SerializeField] private float time = default, lerpTime = default;
    //
    [SerializeField] private int currentQuest = -5;
    //Debug
    [SerializeField] private int currentQuestPref = -5;
    public int resetCrtQuest = default;

    //Tmp
    [SerializeField] private TextMeshProUGUI tilte, description, awards;
    [SerializeField] private GameObject popUp;

    //IsNearWater
    public bool isNearOfWater = false;
    public bool isOut = false;
    public enum QuestValidation
    {   
        CloseUI,
        Maman,
        Medoc,
        TV,
        LIT,
        Sammy,
        News,
        Stats,
        Store,
        AllNews,
        Lac,
        NearOfWater,
        Out 
    };
    public  QuestValidation questvalidationType;

    //endgame
    [SerializeField] private GameObject endPannel;
    [SerializeField] private bool finished = false;


    // Start is called before the first frame update
    void Start()
    {
        Quests.Clear();
        foreach(Transform child in queslistParent.transform)
        {
            Quests.Add(child.gameObject);
        }
        endPannel.SetActive(false);
        questButton = transform.GetChild(0).Find("Button").gameObject;

        //if (currentQuest != 0)
        //{
        //    originalPosition = queslistParent.transform.GetChild(0).gameObject.transform;
        //}
        //else
        //{
        //    originalPosition = queslistParent.transform.GetChild(1).gameObject.transform;
        //}

        CloseQuestPopUp();

        //Debug
        if (PlayerPrefs.HasKey("CrtQuestNumber"))
        {
            currentQuestPref = PlayerPrefs.GetInt("CrtQuestNumber");
            currentQuest = PlayerPrefs.GetInt("CrtQuestNumber");
            Debug.Log("HAS KEY:" + PlayerPrefs.GetInt("CrtQuestNumber") + "|| currentQuestPref" + currentQuestPref);
          
        }
        else
        {
            Debug.LogError("Pas de clef");
        }


        //FIN
        if (PlayerPrefs.GetInt("CrtQuestNumber") == -20)
        {
            //HideQuest();
            questButton.SetActive(false);

        }
        else
        //Init
        if (currentQuest < 0)
        {
            ChangeQuestGO(0);
            //Show First Time
            ShowQuest();
        }
        else
        {
            ChangeQuestGO(currentQuest);
            ShowQuest();
        }

       
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.O))
        //{
        //    Debug.Log("JZ CO Pressed Change Quest");
        //    ChangeQuestGO(1);
        //}

        //if (Input.GetKeyDown(KeyCode.R))
        //{
        //    Debug.Log("JZ R Pressed Reset Quest");
        //    currentQuest = 0;
        //    GameObject.FindObjectOfType<MonneyScript>().GetComponent<MonneyScript>().setMonney(0);
        //    FillQuestPopUp(0);
        //    ShowQuestPopUp(3);
        //    ChangeQuestGO(1);
        //    PlayerPrefs.SetInt("CrtQuestNumber", 0);
        //}

        if (Input.GetKeyDown(KeyCode.R))
        {
            currentQuestPref = PlayerPrefs.GetInt("CrtQuestNumber");
            currentQuest = currentQuestPref;
            Debug.Log("R Pressed update currentQuestPref:"+ currentQuestPref);
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            PlayerPrefs.SetInt("CrtQuestNumber", resetCrtQuest);
            currentQuestPref = PlayerPrefs.GetInt("CrtQuestNumber");
            currentQuest = currentQuestPref;
            Debug.Log("T Pressed Reset currentQuestPref:" + currentQuestPref);
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            PlayerPrefs.DeleteKey("CrtQuestNumber");
            currentQuest = -5;
            Debug.Log("Z Pressed Delete currentQuestPref:" + currentQuestPref);
        }

        //Transition
        SlidePopUpQuest();
    }

    //Getter
    public int GetCurrentQuestNumber()
    {
        return currentQuest;
    }

    private void SlidePopUpQuest()
    {
        if (canMove)
        {
            //ghostTarget.position = Vector3.Lerp(ghostTarget.position, IKTargetToGrab.transform.position, .05f);
            //Debug.Log("disctance: " + Vector3.Distance(pannelToMove.transform.position, targetPosition.position));
            time += Time.deltaTime;

            if (time > lerpTime)
            {
                time = lerpTime;
            }

            //float lerpRatio = time / lerpTime;
            //Debug.Log("JZ Animation Quest : " + Vector3.Distance(pannelToMove.transform.position, targetPosition.position));
            if (Vector3.Distance(pannelToMove.transform.position, targetPosition.position) > 1f)
            {
                pannelToMove.transform.position = Vector3.Lerp(pannelToMove.transform.position, targetPosition.position, /*lerpRatio*/ .25f);
            }
            else
            {
                pannelToMove.transform.position = targetPosition.position;
                canMove = false;
            }
        }
    }


    /*
     * POPUP QUEST
     */

    /// <summary>
    /// Incremente questID
    /// </summary>
    /// <param name="pieces"></param>
    public void ShowQuestPopUp( int pieces)
    {
        if (!popUpShow)
        {
            popUpShow = true;
           
            popUp.SetActive(true);
            GameObject.FindObjectOfType<MonneyScript>().GetComponent<MonneyScript>().monneyVariation(pieces);
            //currentQuest += 1;
            PlayerPrefs.SetInt("CrtQuestNumber", currentQuest);

            Debug.Log("JZ CO Pressed Change Quest : Current id :" + currentQuest);
            
        }   
    }


    public void CloseQuestPopUp()
    {
        popUp.SetActive(false);
        popUpShow = false;
        questChecking = false;
        if(GetCurrentQuestNumber() == 9)
        {
            GameObject.FindObjectOfType<DetectAllNeurolonews>().GetComponent<DetectAllNeurolonews>().CheckAllNewsQuestValidation();
        }
        if (finished)
        {
            ShowEndGamePopUp();
        }
       

    }

    /*
     * Quest Button
     */
    public void ShowQuest()
    {
        questButton.SetActive(false);
        ChangeTarget(targetPosition.position);
        canMove = true;
    }


    public void QuestValidtionHideQuest()
    {
        if (currentQuest == 0)
        {
            ValideteQuest(QuestValidation.CloseUI);
        }
    }

    public void HideQuest()
    {
        canMove = false;
        pannelToMove.transform.localPosition = position;
        questButton.SetActive(true);
    }

    /*
     * Change Quest System
     */
    //Change position where move the popup.
    public void ChangeTarget(Vector3 nextTarget)
    {
        targetPosition.position = nextTarget;
    }

    public void ChangeQuestGO(int id)
    {
        Debug.Log("Quest Go id : " + id);
        if (id < Quests.Count)
        {
            PlayerPrefs.SetInt("CrtQuestNumber", currentQuest);
            PlayerPrefs.Save();
            pannelToMove = Quests[id];
        }
        else
        {
            //Dans le cas ou c'est fini
            Debug.Log("PK Tu passes ici");
            HideQuest();
            questButton.SetActive(false);
            currentQuest = -1;
            //PlayerPrefs.SetInt("CrtQuestNumber", currentQuest);
        }
    }
    /// <summary>
    /// Fill The PoPuP
    /// </summary>
    /// <param name="id">Give the index of quest in Quets list</param>
    public void FillQuestPopUp(int id)
    {
        //HideQuest();
        if(id < Quests.Count)
        {
            Debug.Log("Quest Pop::" +Quests[id].name );

            //tilte.text = Quests[id].GetComponent<FillQuest>().title.text;
            //description.text = Quests[id].transform.GetChild(1).GetComponent<TextMeshProUGUI>().text;
            //awards.text = Quests[id].transform.GetChild(2).GetComponent<TextMeshProUGUI>().text;

            tilte.text = "Bravo tu as r�ussi l'objectif:";
            description.text = Quests[id].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text;
            awards.text = "R�compense gagn�e: "+Quests[id].GetComponent<FillQuest>().quest.award + " Pi�ces";
        }
       
    }

    /// <summary>
    /// Condition to show POPUP
    /// </summary>
    /// <param name="questvalidationType"></param>
    public void ValideteQuest(QuestValidation questvalidationType)
    {
        if (!questChecking && PlayerPrefs.GetInt("CrtQuestNumber")!=-1)
        {
            questChecking = true;
            Debug.Log("JZ QUEST SYSTEM : " + questvalidationType + " en " + questvalidationType);
            switch (questvalidationType)
            {
                //Dans l'ordre
                /*Fermer UI
                 *Maman
                 *Medoc
                 *TV
                 *LIT
                 *Sammy
                 *News
                 *Stats
                 *Store
                 *AllNews
                 *Lac
                 */

                //New
                case QuestValidation.CloseUI: 
                    QuestProcess();
                    break;
                case QuestValidation.Maman:
                    QuestProcess();
                    break;
                case QuestValidation.Medoc:
                    QuestProcess();
                    break;
                case QuestValidation.TV: 
                    QuestProcess();
                    break;
                case QuestValidation.LIT:
                    QuestProcess();
                    break;
                case QuestValidation.Sammy:
                    QuestProcess();
                    break;
                case QuestValidation.News:
                    QuestProcess();
                    break;
                case QuestValidation.Stats:
                    QuestProcess();
                    break;
                case QuestValidation.Store:
                    QuestProcess();
                    //Check AllNews
                   
                    break;
                case QuestValidation.AllNews:
                    QuestProcess();
                    break;
                case QuestValidation.Lac:
                    finished = true;
                    QuestProcess();
                    break;
                default:
                    Debug.LogError("Pas de validation Quest valide");
                    break;
            }
        }
        
    }

    private void QuestProcess()
    {
        Debug.Log("Quest Process");
        FillQuestPopUp(currentQuest);
        ShowQuestPopUp(Quests[currentQuest].GetComponent<FillQuest>().quest.award);
        //ChangeQuest
        //if (!finished)
        //{
            currentQuest = currentQuest + 1;
            ChangeQuestGO(currentQuest);
        //}
        //else
        //{
        //    ShowEndGamePopUp();
        //}
    
    }

    //EndGame PopUp
    public void ShowEndGamePopUp()
    {
        currentQuest = -20;
        PlayerPrefs.SetInt("CrtQuestNumber", currentQuest);
        HideQuest();
        questButton.SetActive(false);
        endPannel.SetActive(true);
    }


    public void CloseEndGametPopUp()
    {
        endPannel.SetActive(false);
    }
}
