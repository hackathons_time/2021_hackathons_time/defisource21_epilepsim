using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonneyScript : MonoBehaviour
{
    [SerializeField] private float monney = 10;
    private float currentProductPrice;
    private GameObject statusBar;

    // Start is called before the first frame update
    void Start()
    {
        statusBar = GameObject.FindObjectOfType<StatusBarsManager>().gameObject;
        //Check State
        if (PlayerPrefs.HasKey("Money"))
        {
            monney = PlayerPrefs.GetFloat("Money");
            GameObject.FindObjectOfType<StatusBarsManager>().GetComponent<StatusBarsManager>().SetMoneyBag(monney);
        }
        else
        {
            setMonney(monney);
            PlayerPrefs.SetFloat("Money", monney);
            GameObject.FindObjectOfType<StatusBarsManager>().GetComponent<StatusBarsManager>().SetMoneyBag(monney);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool checkAvailableProduct(Product prod)
    {
        if (prod.price <= monney)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static string formatPrice(float price)
    {
        string formatedPrice = "";

        if (price > 1)
        {
            formatedPrice = String.Format("{0:0.##}", price) + " pieces";
        }
        else
        {
            formatedPrice = String.Format("{0:0.##}", price) + " piece";
        }

        return formatedPrice;
    }

    public float getMonney()
    {
        return monney;
    }

    public void setMonney(float fric)
    {
        monney = fric;
        PlayerPrefs.SetFloat("Money", monney);
        if (statusBar.activeInHierarchy)
        {
            GameObject.FindObjectOfType<StatusBarsManager>().GetComponent<StatusBarsManager>().SetMoneyBag(monney);
        }
       
    }

    /// <summary>
    /// Augmente/Diminue l'argent du personnage en fonction du montant pass� en param�tre (valeur positive pour augmenter, n�gative pour diminuer)
    /// </summary>
    /// <param name="fric"></param>
    public void monneyVariation(float fric)
    {
        float actualMoney = getMonney();
        setMonney(actualMoney += fric);
       
    }
}























































































































































































































// La l�gende raconte que celui qui lira ce commentaire gagnera une RTX 3090
