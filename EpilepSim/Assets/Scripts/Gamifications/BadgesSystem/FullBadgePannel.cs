using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FullBadgePannel : MonoBehaviour
{
    [SerializeField] private Image logo = default;
    [SerializeField] private TextMeshProUGUI title = default;
    [SerializeField] private TextMeshProUGUI description = default;
    private bool earn;
    [SerializeField] private GameObject awards = default;
    [SerializeField] private TextMeshProUGUI award = default;
    private int level;
    private bool unlock;
    [SerializeField] private Image succesLogo = default;

    //Element UI
    [SerializeField] private GameObject UnlockLogo = default;
    

    public void Start()
    {
       // UnlockLogo.SetActive(false);
    }

    public void OpenAndFillFullBadge(Badge badgeToshow)
    {
        Debug.Log("Open FullBadgePannel");
        logo.sprite = badgeToshow.logo;
        title.text = badgeToshow.title;
        description.text = badgeToshow.description;

        //Conditions parts
        if (badgeToshow.earn)
        {
            award.text = badgeToshow.award+" Pi�ces";
        }
        
        Debug.Log("name : " + badgeToshow.name + " is unlocked:" + badgeToshow.unlock);
        //if (badgeToshow.unlock)
        //{
        UnlockLogo.SetActive(badgeToshow.unlock);
        //}
    }

    public void CloseAndClearFullBadge()
    {
        logo.sprite = default;
        title.text = default;
        description.text = default;
        earn = default;
        award = default;
        level = default;
        unlock = default;
    }
}
