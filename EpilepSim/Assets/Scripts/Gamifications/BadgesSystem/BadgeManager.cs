using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BadgeManager : MonoBehaviour
{

    [SerializeField] private int characterLimit = default;
    [SerializeField] private Transform containerBadges = default;
    [SerializeField] private  Object[] badgeList;
    [SerializeField] private int nbrBadgeInRaw = default;
    [SerializeField] private GameObject fullBadgePanelGO = default;
    [SerializeField] private FullBadgePannel fullBadgePannel = default;
    public int descripstionCharcterLimit = 20;


    public int CharacterLimit
    {
         get { return characterLimit; }
         set { characterLimit = value; }
    }

    // Start is called before the first frame update
    void Start()
    {
        fullBadgePanelGO.SetActive(false);
        //fullBadgePannel = GameObject.Find("FullBadgePannel").GetComponent<FullBadgePannel>();
        InstanciateBadge();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Instanciate Badge pannel with all prefabs in Resources/Badges
    /// </summary>
    private void InstanciateBadge()
    {
        badgeList = Resources.LoadAll("Badges", typeof(GameObject)); 

        foreach (GameObject g in badgeList)
        {
            //Badge(g);

            GameObject badgeClone = Instantiate(g, containerBadges.position, Quaternion.identity);
            badgeClone.transform.SetParent(containerBadges);
            badgeClone.name = g.name;
            badgeClone.transform.localScale = Vector3.one;

            //Take child 0 = image
            //Debug.Log(badgeClone.name+".Child(0)=" + badgeClone.transform.GetChild(0).name, gameObject);
            badgeClone.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(
                delegate
                {
                    AddOpenFullBadgeButton(g);
                }
            );
        }

        UpdateContentSize();
      
    }

    private void AddOpenFullBadgeButton(GameObject g)
    {
        Debug.Log("Add Actions");
        fullBadgePannel.OpenAndFillFullBadge(g.GetComponent<FillBadge>().badge);
        fullBadgePanelGO.SetActive(true);
    }


    /// <summary>
    /// Ajuste heights of content from scrollView
    /// </summary>
    public void UpdateContentSize()
    {
        GridLayoutGroup grid = containerBadges.GetComponent<GridLayoutGroup>();
        RectTransform rect = containerBadges.GetComponent<RectTransform>();
        float rawnbr = (badgeList.Length / nbrBadgeInRaw);
        float heightSize = rawnbr * (grid.cellSize.y + grid.spacing.y);
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, heightSize);
    }

    //Close Full Badge
    public void CloseFullBadgePannel()
    {
        fullBadgePanelGO.SetActive(false);
    }


}
