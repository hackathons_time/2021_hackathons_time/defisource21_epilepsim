using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FillBadge : MonoBehaviour
{
    [SerializeField] public Badge badge = default;
    
    private BadgeManager badgeManager = default;
    private Image logo = default;
    private TextMeshProUGUI title = default;
    private string fullDescription = default;
    private TextMeshProUGUI description = default;
    private bool earn = default;
    private int award = default;
    private int level = default;
    private bool unlock = default;


    // Start is called before the first frame update
    void Start()
    {
        //Get Reference
        badgeManager = GameObject.FindObjectOfType<BadgeManager>().GetComponent<BadgeManager>();
        logo = transform.GetChild(0).GetComponent<Image>();
        title = transform.GetChild(1).GetComponent<TextMeshProUGUI>();
        description = transform.GetChild(2).GetComponent<TextMeshProUGUI>();

        //Fill data for Full Description
        earn = badge.earn;
        award = badge.award;
        level = badge.level;
        unlock = badge.unlock;
        fullDescription = badge.description;

        //Fill visual  data
        logo.sprite = badge.logo;
        title.text = badge.title;
        description.text = fullDescription.Substring(0, badgeManager.descripstionCharcterLimit);//Use Badgemanager to set limits badgeManager.getCharactLmits
    }

}
