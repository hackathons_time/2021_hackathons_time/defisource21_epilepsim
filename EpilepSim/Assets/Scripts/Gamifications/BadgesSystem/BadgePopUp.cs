using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine;

public class BadgePopUp : MonoBehaviour
{
    [SerializeField] private Image logo = default;
    [SerializeField] private TextMeshProUGUI title = default;
  
    private bool earn;
    [SerializeField] private GameObject awards = default;
    [SerializeField] private TextMeshProUGUI award = default;
    [SerializeField] private Image succesLogo = default;

    //Element UI
    [SerializeField] private GameObject UnlockLogo = default;

      [SerializeField] private bool canPopUp = false;


    public void Start()
    {
        // UnlockLogo.SetActive(false);
    }

    public void OpenAndFillFullBadge(Badge badgeToshow)
    {
        Debug.Log("Open FullBadgePannel");
        logo.sprite = badgeToshow.logo;
        title.text = badgeToshow.title;

        //Conditions parts
        if (badgeToshow.earn)
        {
            award.text = badgeToshow.award + " Pi�ces";
        }

        Debug.Log("name : " + badgeToshow.name + " is unlocked:" + badgeToshow.unlock);

        UnlockLogo.SetActive(badgeToshow.unlock);

    }

    public void CloseAndClearFullBadge()
    {
        logo.sprite = default;
        title.text = default;
       
        earn = default;
        award = default;
    }
}
