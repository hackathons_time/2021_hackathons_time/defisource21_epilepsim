using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FullTipsPanel : MonoBehaviour
{
    [SerializeField] private Image logo = default;
    [SerializeField] private TextMeshProUGUI title = default;
    [SerializeField] private TextMeshProUGUI id = default;
    [SerializeField] private TextMeshProUGUI description = default;
  
    public void OpenAndFillFullBadge(TipsProfil tips)
    {
        Debug.Log("Open FullTipsPannel");
        id.text = "N� "+ tips.id;
        logo.color = tips.color;
        title.text = tips.title;
        description.text = tips.description;
    }

    public void CloseAndClearFullBadge()
    {
        logo.sprite = default;
        title.text = default;
        description.text = default;
    }
}
