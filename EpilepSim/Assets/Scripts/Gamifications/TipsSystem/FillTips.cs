using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FillTips : MonoBehaviour
{
    [SerializeField] public TipsProfil tips = default;

    private TipsManager tipsManager = default;
    private Image logo = default;
    private Color color = default;
    private TextMeshProUGUI title = default;
    private TextMeshProUGUI description = default;

    // Start is called before the first frame update
    void Start()
    {
        //Get Reference
        tipsManager = GameObject.FindObjectOfType<TipsManager>().GetComponent<TipsManager>();
        logo = transform.GetChild(0).GetComponent<Image>();
        title = transform.GetChild(1).transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        description = transform.GetChild(1).transform.GetChild(1).GetComponent<TextMeshProUGUI>();

        //Fill visual  data
        logo.color = tips.color;
        title.text = "Astuce " + tips.id;
        description.text = tips.description.Substring(0, tipsManager.descripstionCharcterLimit)+"...";//Use Badgemanager to set limits badgeManager.getCharactLmits
    }
}
