using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TipsManager : MonoBehaviour
{
    [SerializeField] private Transform containerTips = default;
    [SerializeField] private Object[] tipList;
    [SerializeField] private GameObject fullTipsPanelGO = default;
    [SerializeField] private FullTipsPanel fullTipsPannel = default;
    public int descripstionCharcterLimit = 20;
    private float sizeTipContent = default;

    // Start is called before the first frame update
    void Start()
    {
        fullTipsPannel = fullTipsPanelGO.GetComponent<FullTipsPanel>();
        InstanciateTipsInBoard();
    }

    /// <summary>
    /// Instanciate Badge pannel with all prefabs in Resources/Badges
    /// </summary>
    private void InstanciateTipsInBoard()
    {
        tipList = Resources.LoadAll("Tips", typeof(GameObject));

        //Take Height to calculate scrollbar size
        GameObject tmp = (GameObject)tipList[0] as GameObject;
        sizeTipContent= tmp.GetComponent<RectTransform>().sizeDelta.y;

        //Generate list on board
        foreach (GameObject g in tipList)
        {
            //Badge(g);
            GameObject tipClone = Instantiate(g, containerTips.position, Quaternion.identity);
            tipClone.transform.SetParent(containerTips);
            tipClone.name = g.name;
            tipClone.transform.localScale = Vector3.one;

            //Take child 0 = image
            //Debug.Log(badgeClone.name+".Child(0)=" + badgeClone.transform.GetChild(0).name, gameObject);
            //tipClone.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(
            tipClone.transform.GetComponent<Button>().onClick.AddListener(
                delegate
                {
                    AddOpenFullBadgeButton(g);
                }
            );
        }

        UpdateContentSize();

    }

    private void AddOpenFullBadgeButton(GameObject g)
    {
        Debug.Log("Add Actions");
        fullTipsPannel.OpenAndFillFullBadge(g.GetComponent<FillTips>().tips);
        fullTipsPanelGO.SetActive(true);
    }


    /// <summary>
    /// Ajuste heights of content from scrollView to have scrollbar
    /// Calculate by raw the heights
    /// </summary>
    public void UpdateContentSize()
    {
        float heightSize;
        VerticalLayoutGroup vertical = containerTips.GetComponent<VerticalLayoutGroup>();
        RectTransform rect = containerTips.GetComponent<RectTransform>();
        heightSize =  tipList.Length * (sizeTipContent + vertical.spacing);
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, heightSize);
    }

    //Open Full Badge
    public void CloseFullTipsPannel()
    {
        fullTipsPanelGO.SetActive(false);
    }


}
