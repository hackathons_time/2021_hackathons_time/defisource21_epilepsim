using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FullProductPanel : MonoBehaviour
{
    [SerializeField] private Image logo = default;
    [SerializeField] private TextMeshProUGUI title = default;
    [SerializeField] private TextMeshProUGUI description = default;
    [SerializeField] private TextMeshProUGUI price = default;
    [SerializeField] private Button buyButton = default;

    private GameObject player;

    public static Product currentProduct;
    //Faire les gettersSteters

    public void Start()
    {

    }

    public void OpenAndFillFullProduct(Product productToshow)
    {
        logo.sprite = productToshow.logo;
        title.text = productToshow.title;
        description.text = productToshow.description;
        price.text = MonneyScript.formatPrice(productToshow.price);

        currentProduct = productToshow;

        updatePriceDisplay();
    }

    public void CloseAndClearFullProduct()
    {
        logo.sprite = default;
        title.text = default;
        description.text = default;
        price = default;

        currentProduct = null;
    }

    public void updatePriceDisplay()
    {

        if (!player.GetComponent<MonneyScript>().checkAvailableProduct(currentProduct))
        {
            price.color = Color.red;
            buyButton.interactable = false;
        }
        else
        {
            price.color = Color.black;
            buyButton.interactable = true;
        }
    }

    public GameObject getPlayer()
    {
        return player;
    }

    public void setPlayer(GameObject pl)
    {
        player = pl;
    }
}
