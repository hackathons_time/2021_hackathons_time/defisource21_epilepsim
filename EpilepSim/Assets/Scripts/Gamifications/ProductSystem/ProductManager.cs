using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductManager : MonoBehaviour
{
    [SerializeField] private int characterLimit = default;
    [SerializeField] private Transform containerProducts = default;
    [SerializeField] private Object[] productList;
    [SerializeField] private int nbrProductInRaw = default;
    [SerializeField] private GameObject fullProductPanelGO = default;

    private GameObject player;


    public int CharacterLimit
    {
        get { return characterLimit; }
        set { characterLimit = value; }
    }

    // Start is called before the first frame update
    void Start()
    {
        fullProductPanelGO.SetActive(false);
        //fullProductPannel = GameObject.Find("FullProductPannel").GetComponent<FullProductPannel>();
        InstanciateProduct();
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Instanciate Product pannel with all prefabs in Resources/Products
    /// </summary>
    private void InstanciateProduct()
    {
        productList = Resources.LoadAll("Products", typeof(GameObject));

        foreach (GameObject g in productList)
        {
            //Product(g);

            GameObject productClone = Instantiate(g, containerProducts.position, Quaternion.identity);
            productClone.transform.SetParent(containerProducts);
            productClone.name = g.name;
            productClone.transform.localScale = Vector3.one;

            //Take child 0 = image
            //Debug.Log(productClone.name+".Child(0)=" + productClone.transform.GetChild(0).name, gameObject);
            productClone.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(
                delegate
                {
                    AddOpenFullProductButton(g);
                }
            );
        }
        UpdateContentSize();
    }

    private void AddOpenFullProductButton(GameObject g)
    {
        Debug.Log("Add Actions");
        fullProductPanelGO.GetComponent<FullProductPanel>().OpenAndFillFullProduct(g.GetComponent<FillProduct>().product);
        fullProductPanelGO.SetActive(true);
    }

    /// <summary>
    /// Ajuste heights of content from scrollView
    /// </summary>
    public void UpdateContentSize()
    {
        GridLayoutGroup grid = containerProducts.GetComponent<GridLayoutGroup>();
        RectTransform rect = containerProducts.GetComponent<RectTransform>();
        float rawnbr = (productList.Length / nbrProductInRaw);
        float heightSize = rawnbr * (grid.cellSize.y + grid.spacing.y);
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, heightSize);
    }

    public void CloseFullProductPannel()
    {
        fullProductPanelGO.SetActive(false);
    }

    public GameObject GetPlayer()
    {
        return player;
    }

    public void SetPlayer(GameObject pl)
    {
        player = pl;
    }
}
