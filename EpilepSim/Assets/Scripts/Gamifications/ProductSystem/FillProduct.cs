using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class FillProduct : MonoBehaviour
{
    [SerializeField] public Product product = default;
    [SerializeField] private ProductManager productManager = default;

    private Image logo = default;
    private TextMeshProUGUI title = default;
    private string fullDescription = default;
    private TextMeshProUGUI description = default;
    public string fullPrice;
    private TextMeshProUGUI price = default;


    // Start is called before the first frame update
    void Start()
    {
        //Get Reference
        productManager = GameObject.FindObjectOfType<ProductManager>().GetComponent<ProductManager>();
        logo = transform.GetChild(0).GetComponent<Image>();
        title = transform.GetChild(1).GetComponent<TextMeshProUGUI>();
        price = transform.GetChild(2).GetComponent<TextMeshProUGUI>();

        //Fill data for Full Description
        fullDescription = product.description;
        fullPrice = MonneyScript.formatPrice(product.price);

        //Fill visual  data
        logo.sprite = product.logo;
        title.text = product.title;
        price.text = fullPrice;

        if (!productManager.GetPlayer().GetComponent<MonneyScript>().checkAvailableProduct(product))
        {
            price.color = Color.red;
        }
    }
    private void FixedUpdate()
    {
        if (!productManager.GetPlayer().GetComponent<MonneyScript>().checkAvailableProduct(product))
        {
            price.color = Color.red;
        }
    }
}
