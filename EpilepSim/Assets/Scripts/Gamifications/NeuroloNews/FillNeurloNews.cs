using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FillNeurloNews : MonoBehaviour
{
    [SerializeField] public NeurloNewsProfil neurloNewsProfil = default;

    private NeurloNewsManager neurloNewsManager ;
    private Image logo = default;
    private TextMeshProUGUI title = default;
    private TextMeshProUGUI description = default;
    public bool isUnlocked = default;
    public int date = -1;
    public bool needUpdate = false;

    void Start()
    {
        if(!needUpdate){
            //Get Reference
            neurloNewsManager = GameObject.FindObjectOfType<NeurloNewsManager>().GetComponent<NeurloNewsManager>();
            //logo = transform.GetChild(0).GetComponent<Image>();
            var currentParentHavingText = transform.GetChild(0).GetChild(0);
            title = currentParentHavingText.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            description = currentParentHavingText.transform.GetChild(1).GetComponent<TextMeshProUGUI>();

            //title.text = neurloNewsProfil.title; //Reprendre depuis le titre de la Description
            title.text = "Neurolonews ! Le saviez-vous ?";

            //Use NeuroloNewsManager to set limits  neurloNewsManager.getCharactLmits
            description.text = neurloNewsProfil.description; //.Substring(0,  neurloNewsManager.descripstionCharcterLimit);
        }
    }

    public void UpdateProfil(GameObject go, NeurloNewsProfil newNeuro)
    {
        var currentParentHavingText = go.transform.GetChild(0).GetChild(0);
        TextMeshProUGUI _title = currentParentHavingText.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI _description = currentParentHavingText.transform.GetChild(1).GetComponent<TextMeshProUGUI>();

        neurloNewsProfil = newNeuro;
        _title.text = "Neurolonews ! Le saviez-vous ? ";
        _description.text = newNeuro.description.ToString(); //.Substring(0,  neurloNewsManager.descripstionCharcterLimit);
        
    }


}
