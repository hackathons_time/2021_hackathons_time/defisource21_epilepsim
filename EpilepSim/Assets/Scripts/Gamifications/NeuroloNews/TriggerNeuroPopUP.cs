using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerNeuroPopUP : MonoBehaviour
{
   
    [SerializeField] private StatisticsActivities statisticsActivities;
    [SerializeField] private NeurloNewsProfil profil;
    [SerializeField] private bool canPopUp = false;

    [SerializeField] private bool DebugMode = false;
    [SerializeField] private int countNews = 0;
    //[SerializeField] private GameObject Prefab = default;
    //[SerializeField] private GameObject content = default;
    //[SerializeField] private GameObject popup = default;


    private void Start()
    {
        statisticsActivities = GameObject.FindObjectOfType<StatisticsActivities>().GetComponent<StatisticsActivities>();
        Debug.Log("JZ canPopU : " + gameObject.name +" " + canPopUp + " || " + PlayerPrefs.HasKey("NeurolonewsPopUpID_"+profil.id));



        if (!PlayerPrefs.HasKey("NeurolonewsPopUpID_" + profil.id))
        {
            Debug.Log("JZ_ CAN POPUP");
            canPopUp = true;
           
            Debug.Log("JZ canPopU : " + canPopUp + " || " + PlayerPrefs.GetInt("NeurolonewsPopUpID_" + profil.id));
            DebugModeString("JZ canPopU : " + canPopUp + " || " + PlayerPrefs.GetInt("NeurolonewsPopUpID_" + profil.id));


            statisticsActivities = GameObject.FindObjectOfType<StatisticsActivities>().GetComponent<StatisticsActivities>();
        }
        else
        {
            canPopUp = false;
            //Destroy(this.gameObject);
            gameObject.SetActive(false);
            Debug.Log("JZ_ CANT POPUP");
           
            DebugModeString("DESTROX");
        }

        if (DebugMode) canPopUp = true;
      
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
       DebugModeString("Touche by : " + collision.name + "||Tage:"+ collision.tag);
        
       
        Debug.Log("Touche by : " + collision.name);
        

        if (collision.CompareTag("Player") && canPopUp)
        {
            canPopUp = false;
            GameObject.FindObjectOfType<CustomQuestManager>().GetComponent<CustomQuestManager>().HideQuest();
            PlayerPrefs.SetInt("NeurolonewsPopUpID_" + profil.id, 0);
            Debug.Log("JZ_ CAN POPUP And Dectected");

            //statisticsActivities.ActivePopUpDisplay();
            statisticsActivities.ActivePopUpDisplay(profil);
            IncreaseQuestValidation();

            Destroy(this.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("JZ EXIT: " + collision.name);


        if (collision.tag == "Player")
        {
            DebugModeString("JZ_EXIT DESTROX");
            //Destroy(this.gameObject);
        }
    }

    public void DebugModeString(string text)
    {
        if (DebugMode)
        {
            //GameObject badgeClone = Instantiate(Prefab, content.transform.position, Quaternion.identity);
            //badgeClone.transform.SetParent(content.transform);
            //badgeClone.GetComponent<Text>().text = text;
            //badgeClone.transform.localScale = Vector3.one;
        }
    }


    public void ResetPlayerPrefs()
    {
        if (PlayerPrefs.HasKey("NeurolonewsPopUpID_" + profil.id))
        {
            PlayerPrefs.DeleteKey("NeurolonewsPopUpID_" + profil.id);
            canPopUp = true;
            gameObject.SetActive(true);
        }
    }

    public void IncreaseQuestValidation()
    {
        if (!PlayerPrefs.HasKey("NeurolonewsCount"))
        {
            countNews = 1;
            PlayerPrefs.SetInt("NeurolonewsCount", countNews);
        }
        else
        {
            countNews = PlayerPrefs.GetInt("NeurolonewsCount")+ 1;
            PlayerPrefs.SetInt("NeurolonewsCount", countNews);
        }

        Debug.Log("NeurolonewsCount increasing :" + PlayerPrefs.GetInt("NeurolonewsCount"));
       
    }
}
