using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class NeurloNewsManager : MonoBehaviour
{
    [SerializeField] private Transform containerNeurolonews = default;
    [SerializeField] private Object[] neurolonewList = default;
    [SerializeField] private List<GameObject> neurolonewListUnlocked = default;
    [SerializeField] private List<GameObject> neurolonewListLocked = default;
    [SerializeField] private List<GameObject> neurolonewListCrt = new List<GameObject>();
    [SerializeField] private GameObject fullNeurolonewPanelGO = default;
    [SerializeField] public bool isAdding = false;
    private FillNeurloNews fullNeurolonewPannel = default;
    public int descripstionCharcterLimit = 20;
    private float sizeContent = default;
    [Header("Debugs Elements")]
    [SerializeField] public bool useSaveData = false;

    //Save/load System
    private SaveNeuroNewsManager saveNeuroNewsManager = null;


    //Getter/setter
    public Transform GetContainerNeurolonews() { return containerNeurolonews;  }
    public GameObject GetFullNeurolonewPanelGO() { return fullNeurolonewPanelGO; }

    // Start is called before the first frame update
    void Start()
    {
        //Get ref
        fullNeurolonewPannel = fullNeurolonewPanelGO.GetComponent<FillNeurloNews>();
        saveNeuroNewsManager = GetComponent<SaveNeuroNewsManager>();

        InstanciateNeurloNewsInBoard();
    }

    /// <summary>
    /// Instanciate Badge pannel with all prefabs in Resources/Badges
    /// </summary>
    private void InstanciateNeurloNewsInBoard()
    {
        //if (!useSaveData)
        //{
        //    neurolonewList = Resources.LoadAll("Neurolonews", typeof(GameObject));

        //    //Take Height to calculate scrollbar size
        //    //GameObject tmp = (GameObject)neurolonewList[0] as GameObject;
        //    //GameObject tmp = containerNeurolonews.GetChild(0).gameObject;
        //    //sizeContent = tmp.GetComponent<RectTransform>().sizeDelta.y;

        //    //Clear list unlocked NeuroloNews
        //    neurolonewListUnlocked.Clear();

        //    //Generate list on board
        //    //foreach (GameObject g in neurolonewList)
        //    //{
        //    //    //Badge(g);
        //    //    if (g.GetComponent<FillNeurloNews>().isUnlocked)
        //    //    {
        //    //        GameObject tipClone = Instantiate(g, containerNeurolonews.position, Quaternion.identity);
        //    //        tipClone.transform.SetParent(containerNeurolonews);
        //    //        tipClone.name = g.name;
        //    //        tipClone.transform.localScale = Vector3.one;

        //    //        //Add to unlocked Neurolonews
        //    //        neurolonewListUnlocked.Add(tipClone);
        //    //    }
        //    //    else
        //    //    {
        //    //        //Add to locked Neurolonews
        //    //        neurolonewListLocked.Add(g);
        //    //    }
        //    //}

        //    saveNeuroNewsManager.LoadContainerData();

        //    UpdateContentSize();
        //}
        
    }

    public void OpenNeuroloNewsTab()
    {
        neurolonewListCrt = saveNeuroNewsManager.neurolonewListCrt;
        saveNeuroNewsManager.LoadContainerData();
 
        UpdateContentSize();
        Debug.Log("ChildCount:" + containerNeurolonews.childCount+" of: " + containerNeurolonews);
    }


    /// <summary>
    /// Ajuste heights of content from scrollView to have scrollbar
    /// Calculate by raw the heights
    /// </summary>
    public void UpdateContentSize()
    {
        float heightSize;
        //Take Height to calculate scrollbar size
        GameObject tmp = containerNeurolonews.GetChild(0).gameObject;
        sizeContent = tmp.GetComponent<RectTransform>().sizeDelta.y;
        Debug.Log("SizeCellHeight:"+sizeContent);

        VerticalLayoutGroup vertical = containerNeurolonews.GetComponent<VerticalLayoutGroup>();
        RectTransform rect = containerNeurolonews.GetComponent<RectTransform>();
      
        heightSize = neurolonewListCrt.Count * (sizeContent + vertical.spacing);
        Debug.Log("SizeCellHeight:" + sizeContent + " + verticalSapcing:"  + vertical.spacing + "* Childcout:" + containerNeurolonews.childCount + "= heightSize:" + heightSize);
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, heightSize);
    }

    //Open Full Badge
    public void CloseFullTipsPannel()
    {
        fullNeurolonewPanelGO.SetActive(false);
    }

    /*Methode PopUp qui lorsqu'on touche ca d�verrouille l'objet dans la list des Unlock
     * Genre directement l'ajouter � la list et update la scrolview.. 
     */
    public void NewUnlockedNeurolonews(NeurloNewsProfil newProfil)
    {
        Debug.Log("JZ_POPUP New PopUP");
        if (!isAdding)
        {
            Debug.Log("JZ_POPUP IS ADDING");
            isAdding = true;
            //Generate list on board
            //foreach (GameObject g in neurolonewList)
            //{
            //    //Badge(g);
            //    if (g.GetComponent<FillNeurloNews>().neurloNewsProfil == newProfil)
            //    {
            //        GameObject tipClone = Instantiate(g, containerNeurolonews.position, Quaternion.identity);
            //        tipClone.transform.SetParent(containerNeurolonews);
            //        tipClone.name = g.name;
            //        tipClone.transform.localScale = Vector3.one;

            //        //Generate Date

            //        tipClone.GetComponent<FillNeurloNews>().date = int.Parse(System.DateTime.UtcNow.ToString("yyyyMMdd") + FormatNumber(neurolonewListUnlocked.Count));

            //        //Add to unlocked Neurolonews
            //        neurolonewListUnlocked.Add(tipClone);
            //    }
            //}
            // SortByDateDescendant(neurolonewListUnlocked);

            saveNeuroNewsManager.UnlockContainerData(newProfil);
            UpdateContentSize();

            //StartCoroutine(WaitToUnleockBool());
        }
       
        // /!\ attention peut ajouter 2 fois une carte faut un check !! 
    }

    /// <summary>
    /// Format the number to 4 characters -> 1 = 0001
    /// </summary>
    /// <param name="i"></param>
    /// <returns></returns>
    public string FormatNumber(int i)
    {
        /// JZ Using Switch
        //string tmp = "-";
        //switch (i.ToString().Length)
        //{
        //    case 1:
        //        tmp = "000" + i;
        //        break;
        //    case 2:
        //        tmp = "00" + i;
        //        break;
        //    case 3:
        //        tmp = "0" + i;
        //        break;
        //    default:
        //        tmp = i.ToString();
        //        break;

        //}
        //return tmp;

        ///JZ Using stringformat
        return $"{i:00}";
    }

    //Sort by date // Still in progress
    public void SortByDateDescendant(List<GameObject> listToSort)
    {
        List<GameObject> sortedList = new List<GameObject>();

        foreach (GameObject g in listToSort)
        {
            sortedList = neurolonewListUnlocked.OrderBy(go => go.GetComponent<FillNeurloNews>().date).ToList();
        }
        foreach (GameObject g in sortedList)
        {
            Debug.Log("JZ_SortedList child :" + g.name);
        }

    }

    /// <summary>
    /// Search in list of Locked Neurolonews and give one with a random index !
    /// </summary>
    /// <returns></returns>
    public NeurloNewsProfil GetRandomNeurlonewsLocked()
    {
        int randomIndex = Random.Range(0, neurolonewListLocked.Count - 1);
        Debug.Log("JZ_Random choice is Neuro profil : " + neurolonewListLocked[randomIndex].GetComponent<FillNeurloNews>().neurloNewsProfil.id);
        return neurolonewListLocked[randomIndex].GetComponent<FillNeurloNews>().neurloNewsProfil;
    }

    IEnumerator WaitToUnleockBool()
    {
        yield return new WaitForSeconds(1f);
        isAdding = false;
    }
    
}
